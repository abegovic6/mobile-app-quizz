package ba.etf.rma21.projekat.viewmodel

import android.content.Context
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.repositories.KvizRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class KvizViewModel (private val searchDone: ((movies: List<Kviz>) -> Unit)?,
                     private val onError: (()->Unit)?) {
    val scope = CoroutineScope(Job() + Dispatchers.IO)

    fun getMyKvizes(context: Context, onSuccess: (kvizovi: List<Kviz>) -> Unit,
                    onError: () -> Unit) {
        scope.launch {
            KvizRepository.setContext(context)
            val result = KvizRepository.getUpisani()
            when (result) {
                is List<Kviz> -> onSuccess.invoke(result)
                else -> onError.invoke()
            }
        }
    }

    fun getDone(context: Context,onSuccess: (kvizovi: List<Kviz>) -> Unit,
                onError: () -> Unit) {
        scope.launch {
            KvizRepository.setContext(context)
            val result = KvizRepository.getDone()
            when (result) {
                is List<Kviz> -> onSuccess.invoke(result)
                else -> onError.invoke()
            }
        }
    }

    fun getFuture(context: Context, onSuccess: (kvizovi: List<Kviz>) -> Unit,
                  onError: () -> Unit) {
        scope.launch {
            KvizRepository.setContext(context)
            val result = KvizRepository.getFuture()
            when (result) {
                is List<Kviz> -> onSuccess.invoke(result)
                else -> onError.invoke()
            }
        }
    }

    fun getNotTaken(context: Context, onSuccess: (kvizovi: List<Kviz>) -> Unit,
                    onError: () -> Unit) {
        scope.launch {
            KvizRepository.setContext(context)
            val result = KvizRepository.getNotTaken()
            when (result) {
                is List<Kviz> -> onSuccess.invoke(result)
                else -> onError.invoke()
            }
        }
    }

    fun getAll(onSuccess: (kvizovi: List<Kviz>) -> Unit,
               onError: () -> Unit) {
        scope.launch {
            val result = KvizRepository.getAll()
            when (result) {
                is List<Kviz> -> onSuccess.invoke(result)
                else -> onError.invoke()
            }
        }
    }

    fun getStatus (kviz : Kviz) : String {
        return KvizRepository.getStatus(kviz)
    }


}