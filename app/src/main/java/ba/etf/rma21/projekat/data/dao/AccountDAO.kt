package ba.etf.rma21.projekat.data.dao

import androidx.room.*
import ba.etf.rma21.projekat.data.models.Account

@Dao
interface AccountDAO {

    @Query("SELECT * FROM account")
    suspend fun getAccount(): List<Account>?

    @Delete
    fun deleteKorisnik(ac: Account)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAcc(vararg ac: Account)
}