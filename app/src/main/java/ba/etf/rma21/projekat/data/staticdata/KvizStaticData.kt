//package ba.etf.rma21.projekat.data.staticdata
//
//import ba.etf.rma21.projekat.data.models.Kviz
//import java.util.*
//
//fun allKviz(): List<Kviz> {
//        val pocetak1 = Calendar.getInstance()
//        pocetak1.set(2022, 5-1, 4)
//        val pocetak2 = Calendar.getInstance()
//        pocetak2.set(2021, 2-1, 4)
//
//        val kraj1 = Calendar.getInstance()
//        kraj1.set(2022, 6-1, 4)
//        val kraj2 = Calendar.getInstance()
//        kraj2.set(2021, 2-1, 10)
//
//        val izrada1 = null
//        val izrada2 = Calendar.getInstance()
//        izrada2.set(2021, 2-1, 4)
//
//        return listOf(
//                // RMA
//                Kviz("Kviz 1","RMA", pocetak1.time, kraj1.time, izrada1, 5, "RMA1", null),
//                Kviz("Kviz 2","RMA", pocetak2.time, kraj1.time, izrada1,  5, "RMA2", null),
//                Kviz("Kviz 3","RMA", pocetak2.time, kraj1.time, izrada1,  5, "RMA31", null),
//
//                // CCI
//                Kviz("Kviz 1","CCI", pocetak2.time, kraj1.time, izrada1,  5, "CCI1", null),
//                Kviz("Kviz 2","CCI", pocetak2.time, kraj2.time, izrada1,  2, "CCI1", null),
//                Kviz("Kviz 3","CCI", pocetak2.time, kraj1.time, izrada2.time,  5, "CCI1", 2F),
//
//                // OOAD
//                Kviz("Kviz 1","OOAD", pocetak2.time, kraj1.time, izrada1,  5, "OOAD1", null),
//                Kviz("Kviz 2","OOAD", pocetak1.time, kraj1.time, izrada1, 5, "OOAD2", null),
//
//                // TP
//                Kviz("Kviz 1","TP", pocetak2.time, kraj2.time, izrada1,  5, "TP1", null),
//                Kviz("Kviz 2","TP", pocetak1.time, kraj1.time, izrada1, 6, "TP2", null),
//
//                // OBP
//                Kviz("Kviz 1","OBP", pocetak2.time, kraj2.time, izrada1,  5, "OBP1", null),
//                Kviz("Kviz 2","OBP", pocetak1.time, kraj1.time, izrada1, 6, "OBP2", null),
//
//                // AFJ
//                Kviz("Kviz 1","AFJ", pocetak1.time, kraj1.time, izrada1, 5, "AFJ1", null),
//                Kviz("Kviz 2","AFJ", pocetak2.time, kraj2.time, izrada1,  20, "AFJ2", null),
//
//                // RA
//                Kviz("Kviz 1","RA", pocetak2.time, kraj2.time, izrada1,  8, "RA2", null),
//                Kviz("Kviz 2","RA", pocetak2.time, kraj2.time, izrada1,  5, "RA2", null),
//                Kviz("Kviz 3","RA", pocetak1.time, kraj1.time, izrada1, 6, "RA1", null),
//
//                // RPR
//                Kviz("Kviz 1","RPR", pocetak1.time, kraj1.time, izrada1, 5, "RPR1", null),
//                Kviz("Kviz 2","RPR", pocetak2.time, kraj2.time, izrada1,  20, "RPR2", null),
//
//                // DM
//                Kviz("Kviz 1","DM", pocetak1.time, kraj1.time, izrada1, 5, "DM1", null),
//                Kviz("Kviz 2","DM", pocetak2.time, kraj2.time, izrada1,  20, "DM2", null)
//
//
//
//
//        )
//}