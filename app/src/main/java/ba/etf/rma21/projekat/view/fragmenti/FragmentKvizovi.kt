package ba.etf.rma21.projekat.view.fragmenti

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma21.projekat.MainActivity
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.view.recyclerdapter.KvizListAdapter
import ba.etf.rma21.projekat.view.arrayadapter.FilterSpinnerAdapter
import ba.etf.rma21.projekat.viewmodel.KvizViewModel
import ba.etf.rma21.projekat.viewmodel.PitanjaKvizViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.util.*

class FragmentKvizovi : Fragment(){
    private lateinit var filterKvizova: Spinner
    private lateinit var listaKvizova: RecyclerView
    private lateinit var listaKvizovaAdapter: KvizListAdapter

    private val kvizViewModel = KvizViewModel(null, null)
    private val pitanjaKvizViewModel = PitanjaKvizViewModel()

    private var listaPitanja = listOf<Pitanje>()
    private lateinit var kvizGlobalno: Kviz



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_kvizovi, container, false)


        val activity = activity as MainActivity
        val menu = activity.getBottomNavigation().menu
        menu.findItem(R.id.kvizovi).isVisible = true
        menu.findItem(R.id.predmeti).isVisible = true
        menu.findItem(R.id.predajKviz).isVisible = false
        menu.findItem(R.id.zaustaviKviz).isVisible = false
        menu.findItem(R.id.kvizovi).isChecked = true

        listaKvizova = view.findViewById(R.id.listaKvizova)
        filterKvizova = view.findViewById(R.id.filterKvizova)

        setUpListaKvizova()
        setUpFilterKvizova()

        bundlePredmetiDugme()

        context?.let {kvizViewModel.getMyKvizes(context = it,
            onSuccess = ::onSuccess,
            onError = ::onError
        ) }
        return view
    }

    fun onSuccess(quizzes:List<Kviz>){
        GlobalScope.launch(Dispatchers.IO){
            withContext(Dispatchers.Main){
                listaKvizovaAdapter.updateKvizovi(quizzes)
            }
        }
    }
    fun onSuccessPitanja(quizzes:List<Pitanje>){
        GlobalScope.launch(Dispatchers.IO){
            withContext(Dispatchers.Main){
                listaPitanja = quizzes
                pokreniKviz2(kvizGlobalno)
            }
        }
    }
    fun onError() {
        val toast = Toast.makeText(context, "Search error", Toast.LENGTH_SHORT)
        toast.show()
    }


    private fun setUpListaKvizova () {
        listaKvizova.layoutManager = GridLayoutManager(listaKvizova.context, 2)
        listaKvizovaAdapter =
                KvizListAdapter(arrayListOf()) { kviz ->
                    pokreniKviz(kviz)
                }
        listaKvizova.adapter = listaKvizovaAdapter
        kvizViewModel.getMyKvizes(context = listaKvizova.context,onSuccess = ::onSuccess, onError = ::onError)
    }

    private fun provjeriOtvaranje(kviz: Kviz) : Boolean {
        if(filterKvizova.selectedItemPosition == 1) return false
        val status = kvizViewModel.getStatus(kviz)
        if(status == "zuta") return false
        if(listaPitanja.isEmpty()) return false
        return true
    }

    fun stringToDate(string: String?): Date?{
        if(string == null)
            return null
        val format = SimpleDateFormat("yyyy-MM-dd")
        return format.parse(string)
    }

    private fun pokreniKviz2(kviz: Kviz) {
        if(provjeriOtvaranje(kviz)) {
            val fragment = FragmentPokusaj.newInstance(listaPitanja)
            val bundle = Bundle().apply {
                putSerializable("kviz", kviz)
                putBoolean("zavrsen", kviz.datumRada != null || (kviz.datumKraj != null && stringToDate(kviz.datumKraj)!!.before(Date())))
            }
            fragment.arguments = bundle
            replaceFragment(fragment)
        } else {
            val t = Toast.makeText(listaKvizova.context, "Nije moguće pokrenuti kviz", Toast.LENGTH_SHORT)
            t.show()
        }
    }
    private fun pokreniKviz (kviz: Kviz) {
        kvizGlobalno = kviz
        pitanjaKvizViewModel.getPitanja(id = kviz.id!!, onSuccess = ::onSuccessPitanja, onError = ::onError, context = context!!)
    }

    private fun replaceFragment(fragment: Fragment) {
        val transaction = fragmentManager?.beginTransaction()
        if (transaction != null) {
            transaction.replace(R.id.container, fragment)
            transaction.addToBackStack(null)
            transaction.commit()
        }
    }

    private fun setUpFilterKvizova () {
        val adapter = ArrayAdapter.createFromResource(listaKvizova.context, R.array.FilterKvizova, R.layout.item_spinner)
        filterKvizova.adapter = adapter
        filterKvizova.onItemSelectedListener =
                FilterSpinnerAdapter { position ->
                    filtriranjeOnClick(position)
                }
    }

    private fun filtriranjeOnClick (position: Int) {
        when (position) {
            0 -> context?.let {kvizViewModel.getMyKvizes(context = it,
                onSuccess = ::onSuccess,
                onError = ::onError
            ) }
            1 -> kvizViewModel.getAll(onSuccess = ::onSuccess, onError = ::onError)
            2 -> context?.let {kvizViewModel.getDone(context = it,
                onSuccess = ::onSuccess,
                onError = ::onError
            ) }
            3 -> context?.let {kvizViewModel.getFuture(context = it,
                onSuccess = ::onSuccess,
                onError = ::onError
            ) }
            4 -> context?.let {kvizViewModel.getNotTaken(context = it,
                onSuccess = ::onSuccess,
                onError = ::onError
            ) }
            else -> {
                context?.let {kvizViewModel.getMyKvizes(context = it,
                    onSuccess = ::onSuccess,
                    onError = ::onError
                ) }
            }
        }
    }

    private fun bundlePredmetiDugme() {
        filtriranjeOnClick(filterKvizova.selectedItemPosition)
    }

    companion object {
        fun newInstance(): FragmentKvizovi = FragmentKvizovi()
    }
}