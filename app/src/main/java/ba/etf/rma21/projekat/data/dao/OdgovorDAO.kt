package ba.etf.rma21.projekat.data.dao

import androidx.room.*
import ba.etf.rma21.projekat.data.models.Odgovor

@Dao
interface OdgovorDAO {
    @Delete
    fun deleteOdgovori(odg: Odgovor)

    @Query("SELECT * FROM odgovor WHERE  KvizTakenId LIKE :idKviza")
    suspend fun getOdgovori(idKviza: Int): List<Odgovor>

    @Query("SELECT * FROM odgovor")
    suspend fun getSviOdgovori(): List<Odgovor>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOdgovor(vararg kvizovi: Odgovor)


}