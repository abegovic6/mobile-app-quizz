package ba.etf.rma21.projekat.data.dao

import androidx.room.*
import ba.etf.rma21.projekat.data.models.Grupa

@Dao
interface GrupaDAO {

    @Query("SELECT * FROM grupa")
    suspend fun getGrupe(): List<Grupa>

    @Delete
    fun deleteGrupe(g: Grupa)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertGrupa(vararg kvizovi: Grupa)
}