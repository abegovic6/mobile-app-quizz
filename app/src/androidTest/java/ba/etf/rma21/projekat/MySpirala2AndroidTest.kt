//package ba.etf.rma21.projekat
//
//import androidx.recyclerview.widget.RecyclerView
//import androidx.test.espresso.Espresso
//import androidx.test.espresso.action.ViewActions.click
//import androidx.test.espresso.assertion.ViewAssertions
//import androidx.test.espresso.contrib.NavigationViewActions
//import androidx.test.espresso.contrib.RecyclerViewActions
//import androidx.test.espresso.intent.rule.IntentsTestRule
//import androidx.test.espresso.matcher.ViewMatchers.*
//import androidx.test.ext.junit.runners.AndroidJUnit4
//import ba.etf.rma21.projekat.data.repositories.PitanjeKvizRepository
//import org.hamcrest.CoreMatchers.*
//import org.junit.Rule
//import org.junit.Test
//import org.junit.runner.RunWith
//
//@RunWith(AndroidJUnit4::class)
//class MySpirala2AndroidTest {
//    @get:Rule
//    val intentsTestRule = IntentsTestRule<MainActivity>(MainActivity::class.java)
//
//    @Test
//    fun zadatakJedan() {
//        Espresso.onView(withId(R.id.predmeti)).perform(click())
//        Espresso.onView(withId(R.id.odabirGodina)).perform(click())
//        Espresso.onData(allOf(`is`(instanceOf(String::class.java)), `is`("3"))).perform(click())
//        Espresso.onView(withId(R.id.odabirPredmet)).perform(click())
//        Espresso.onData(allOf(`is`(instanceOf(String::class.java)), `is`("OBP"))).perform(click())
//        Espresso.onView(withId(R.id.odabirGrupa)).perform(click())
//        Espresso.onData(allOf(`is`(instanceOf(String::class.java)), `is`("OBP1"))).perform(click())
//        Espresso.onView(withId(R.id.kvizovi)).perform(click())
//        Espresso.onView(withId(R.id.predmeti)).perform(click())
//
//        Espresso.onView(withId(R.id.odabirGodina)).check(ViewAssertions.matches(withSpinnerText(containsString("3"))))
//        Espresso.onView(withId(R.id.odabirPredmet)).check(ViewAssertions.matches(withSpinnerText(containsString("OBP"))))
//        Espresso.onView(withId(R.id.odabirGrupa)).check(ViewAssertions.matches(withSpinnerText(containsString("OBP1"))))
//
//        Espresso.onView(withId(R.id.dodajPredmetDugme)).perform(click())
//        Espresso.onView(withId(R.id.kvizovi)).perform(click())
//        Espresso.onView(withId(R.id.predmeti)).perform(click())
//
//        Espresso.onView(withId(R.id.odabirGodina)).check(ViewAssertions.matches(withSpinnerText(containsString("3"))))
//        Espresso.onView(withId(R.id.odabirPredmet)).check(ViewAssertions.matches(withSpinnerText(containsString(""))))
//        Espresso.onView(withId(R.id.odabirGrupa)).check(ViewAssertions.matches(withSpinnerText(containsString(""))))
//    }
//
//    @Test
//    fun zadatakDva() {
//        Espresso.onView(withId(R.id.kvizovi)).perform(click())
//        Espresso.onView(withId(R.id.filterKvizova)).perform(click())
//        Espresso.onData(allOf(`is`(instanceOf(String::class.java)), `is`("Svi moji kvizovi"))).perform(click())
//        Espresso.onView(withId(R.id.listaKvizova)).perform(RecyclerViewActions.actionOnItem<RecyclerView.ViewHolder>(
//            allOf(hasDescendant(withText("Kviz 1")),
//                hasDescendant(withText("CCI"))), click()))
//
//        val pitanja = PitanjeKvizRepository.getPitanja("Kviz 1", "CCI")
//        var i = 0
//        for (pitanje in pitanja) {
//            Espresso.onView(withId(R.id.navigacijaPitanja)).perform(NavigationViewActions.navigateTo(i)).perform(click())
//            Espresso.onView(withId(R.id.tekstPitanja)).check(ViewAssertions.matches(withText(pitanja[i].tekst)))
//            i++
//        }
//        Espresso.onView(withId(R.id.predajKviz)).perform(click())
//        Espresso.onView(withSubstring("Završili ste kviz")).check(ViewAssertions.matches(isDisplayed()))
//        Espresso.onView(withId(R.id.predajKviz)).check(ViewAssertions.matches(not(ViewAssertions.matches(isDisplayed()))))
//        Espresso.onView(withId(R.id.zaustaviKviz)).check(ViewAssertions.matches(not(ViewAssertions.matches(isDisplayed()))))
//
//        Espresso.onView(withText("Rezultat")).check(ViewAssertions.matches(isDisplayed()))
//        Espresso.onView(withId(R.id.navigacijaPitanja)).perform(NavigationViewActions.navigateTo(0)).perform(click())
//        Espresso.onView(withText("Rezultat")).perform(click())
//        Espresso.onView(withSubstring("Završili ste kviz")).check(ViewAssertions.matches(isDisplayed()))
//
//    }
//}