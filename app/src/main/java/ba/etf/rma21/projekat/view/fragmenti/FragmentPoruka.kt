package ba.etf.rma21.projekat.view.fragmenti

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.R

class FragmentPoruka : Fragment() {
    private lateinit var tvPoruka : TextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_poruka, container, false)

        requireActivity().onBackPressedDispatcher.addCallback(this,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    val fragmentKvizovi = FragmentKvizovi.newInstance()
                    openFragment(fragmentKvizovi)
                }
            })

        tvPoruka = view.findViewById(R.id.tvPoruka)
        bundle()

        return view
    }

    companion object {
        fun newInstance(): FragmentPoruka = FragmentPoruka()
    }

    private fun openFragment(fragment: Fragment) {
        val transaction = fragmentManager?.beginTransaction()
        if (transaction != null) {
            transaction.replace(R.id.container, fragment)
            transaction.addToBackStack(null)
            transaction.commit()
        }
    }

    private fun bundle() {
        val bundle = this.arguments
        if(bundle != null) {
            tvPoruka.text = bundle.getString("tekst")
        }
    }


}