package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.models.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.time.LocalDateTime
import kotlin.math.roundToInt

class OdgovorRepository {
    companion object {
        fun setContext(_context: Context){
            DBRepository.setContext(_context)
        }

        suspend fun getOdgovoriKvizApi(idKviza: Int?) : List<Odgovor>? {
            return withContext(Dispatchers.IO) {
                var kvizTaken = ApiAdapter.retrofit.getKvizTakenZaStudenta(AccountRepository.getHash())
                var kvizTakenId = -1
                for(kv in kvizTaken) {
                    if(kv.KvizId == idKviza) {
                        kvizTakenId = kv.id!!
                    }
                }

                if(kvizTakenId == -1) return@withContext null

                var odgovori = ApiAdapter.retrofit.getOdgovorZaKviz(AccountRepository.getHash(), kvizTakenId)
                for(odg in odgovori) {
                    odg.kvizTakebId = idKviza!!
                }

                return@withContext odgovori
            }
        }

        suspend fun getOdgovoriKviz(idKviza: Int?) : List<Odgovor>? {
            return withContext(Dispatchers.IO) {
                var kvizTaken = DBRepository.getKvizTaken().firstOrNull{kt -> kt.KvizId == idKviza}
                if(kvizTaken == null) return@withContext emptyList<Odgovor>()
                var odgovori = DBRepository.getOdgovori().filter { o-> o.kvizTakebId == kvizTaken.id }
                return@withContext odgovori
            }
        }

        suspend fun daLiJeZavrsen(kviz: Kviz) : Boolean {
            return withContext(Dispatchers.IO) {
                var odgovori = getOdgovoriKvizApi(kviz.id)
                var pitanje = PitanjeKvizRepository.getPitanjaApi(kviz.id)


                if (pitanje != null) {
                    if (odgovori != null) {
                        return@withContext odgovori.size == pitanje.size
                    } else return@withContext false
                } else return@withContext false

            }
        }


        suspend fun odgovoriNaOstatak(kviz: Kviz) {
            return withContext(Dispatchers.IO) {
                var odgovori = getOdgovoriKviz(kviz.id)
                var pitanja = PitanjeKvizRepository.getPitanjaApi(kviz.id)
                var kvizTaken = TakeKvizRepository.zapocniKviz(kviz.id)

                if (pitanja != null) {
                    for(pitanje in pitanja) {
                        var odgvoren = false
                        if (odgovori != null) {
                            for (odgovor in odgovori) {
                                if(odgovor.pitanjeId == pitanje.id)
                                    odgvoren = true
                            }
                        }
                        if(!odgvoren) {
                            postaviOdgovorKvizApi(kvizTaken!!.id, pitanje.id!!, pitanje.opcije.size)
                        }
                    }
                }

                return@withContext
            }
        }

        suspend fun predajOdgovore(idKviz: Int) {
            return withContext(Dispatchers.IO) {

                val kvizTaken = DBRepository.getKvizTaken().firstOrNull() { k-> k.KvizId == idKviz }
                val kviz = DBRepository.getUpisaneKvizove().first { k-> k.id == idKviz }

                if(kvizTaken == null) {
                    odgovoriNaOstatak(kviz)
                    return@withContext
                }

                val odgovoriSvi = DBRepository.getOdgovori().filter { o-> o.kvizTakebId == kvizTaken.id }

                var bodovi: Int? = null
                for(odg in odgovoriSvi) {
                    bodovi = postaviOdgovorKvizApi(kvizTaken.id, odg.pitanjeId, odg.odgovoreno)
                }

                if (bodovi == null) {
                    kviz.osvojeniBodovi = 0F
                } else {
                    kviz.osvojeniBodovi = bodovi.toFloat()
                }
                kviz.datumRada = DBRepository.dajDate(LocalDateTime.now())
                kviz.predan = true

                DBRepository.dodajKviz(kviz)

                odgovoriNaOstatak(kviz)
            }

        }

        suspend fun postaviOdgovorKviz(idKvizTaken: Int, idPitanje: Int, odgovor: Int): Int? {
            return withContext(Dispatchers.IO) {
                val odgovoriSvi = DBRepository.getOdgovori()
                val pitanja = DBRepository.getPitanja()
                val kvizTaken = DBRepository.getKvizTaken()

                var kvizId = 0

                for(kT in kvizTaken) {
                    if (kT.id == idKvizTaken) {
                        kvizId = kT.KvizId!!
                        break
                    }
                }



                val pitanjaZaKviz = pitanja.filter { p-> p.KvizId == kvizId }
                val odgovoriZaKviz = odgovoriSvi.filter { o-> o.kvizTakebId == idKvizTaken }

                var brojTacnih = 0
                var odgovorenoVec = false
                for(pitanje in pitanjaZaKviz){
                    if(pitanje.id == idPitanje && pitanje.tacan == odgovor) {
                        brojTacnih++
                        continue
                    }
                    for(odgovorSvi in odgovoriZaKviz){
                        if(idPitanje == odgovorSvi.pitanjeId) {
                            odgovorenoVec = true
                        }
                        if(pitanje.id == odgovorSvi.pitanjeId && pitanje.tacan == odgovorSvi.odgovoreno)
                            brojTacnih++
                    }
                }

                var ukupniBodovi = (brojTacnih*100.0/pitanjaZaKviz.size).roundToInt()

                if(odgovorenoVec) return@withContext ukupniBodovi

                val odgovoreno = Odgovor(odgovoriZaKviz.size+1, odgovor, idPitanje, idKvizTaken)

                DBRepository.dodajOdgovor(odgovoreno)
                return@withContext ukupniBodovi
            }
        }

        suspend fun postaviOdgovorKvizApi(idKvizTaken: Int?, idPitanje: Int?, odgovor: Int?): Int? {
            return withContext(Dispatchers.IO) {
                var hash = AccountRepository.getHash()
//                var kvizoviPokrenuti = ApiAdapter.retrofit.getTakenKvizes(hash)
                var kvizoviPokrenuti = TakeKvizRepository.getPocetiKvizovi()

                println(idKvizTaken)
                println(idPitanje)

                var kvizId = 0
                if (kvizoviPokrenuti != null) {
                    for(kt in kvizoviPokrenuti){
                        if(kt.id == idKvizTaken) {
                            kvizId = kt.KvizId!!
                        }
                    }
                }
                var ukupniBodovi = 0
                var odgovori = ApiAdapter.retrofit.getOdgovorZaKviz(hash, idKvizTaken!!)

                var pitanja = PitanjeKvizRepository.getPitanjaApi(kvizId)
                var brojPitanja = pitanja!!.size
                var tacno = false
                var brojTacnih = 0
                var bodovi = 0
                var odgovorenoVec = false
                for(pitanje in pitanja){
                    if(pitanje.id == idPitanje && pitanje.tacan == odgovor) {
                        tacno = true
                        brojTacnih++
                    }
                    for(odgovorSvi in odgovori){
                        if(idPitanje == odgovorSvi.pitanjeId) {
                            odgovorenoVec = true
                        }

                        if(pitanje.id == odgovorSvi.pitanjeId && pitanje.tacan == odgovorSvi.odgovoreno)
                            brojTacnih++
                    }
                }
                if(tacno)
                    bodovi = (100.0/brojPitanja).roundToInt()
                ukupniBodovi = (brojTacnih*100.0/brojPitanja).roundToInt()

                if(odgovorenoVec) return@withContext ukupniBodovi

                val odgovoreno = OdgovorenoPitanje(odgovor!!, idPitanje!!, ukupniBodovi)
                var response = ApiAdapter.retrofit.postaviOdgovor(hash, idKvizTaken, odgovoreno)

                return@withContext ukupniBodovi
            }
        }




    }
}