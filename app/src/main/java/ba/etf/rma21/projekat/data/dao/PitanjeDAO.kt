package ba.etf.rma21.projekat.data.dao

import androidx.room.*
import ba.etf.rma21.projekat.data.models.Pitanje

@Dao
interface PitanjeDAO {
    @Query("SELECT * FROM pitanje WHERE KvizId LIKE :idKviza")
    suspend fun getPitanja(idKviza: Int): List<Pitanje>

    @Query("SELECT * FROM pitanje")
    suspend fun getSvaPitanja(): List<Pitanje>

    @Delete
    fun deletePitanja(pitanje: Pitanje)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPitanja(vararg kvizovi: Pitanje)
}