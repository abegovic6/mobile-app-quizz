//package ba.etf.rma21.projekat
//
//import ba.etf.rma21.projekat.data.models.Predmet
//import ba.etf.rma21.projekat.viewmodel.GrupaViewModel
//import org.hamcrest.MatcherAssert
//import org.hamcrest.Matchers.*
//import org.hamcrest.CoreMatchers.`is` as Is
//import org.junit.Test
//
//class GrupaUnitTest {
//    val grupaViewModel = GrupaViewModel()
//
//    @Test
//    fun getGroupsByPredmetTest() {
//        var grupe = grupaViewModel.getGroupsByPredmet("CCI")
//        MatcherAssert.assertThat(grupe, hasItem<Predmet>(hasProperty("naziv", Is("CCI1")))
//        )
//        MatcherAssert.assertThat(grupe, hasItem<Predmet>(hasProperty("naziv", Is("CCI2")))
//        )
//
//        grupe = grupaViewModel.getGroupsByPredmet("RMA")
//        MatcherAssert.assertThat(grupe, hasItem<Predmet>(hasProperty("naziv", Is("RMA1")))
//        )
//        MatcherAssert.assertThat(grupe, hasItem<Predmet>(hasProperty("naziv", Is("RMA2")))
//        )
//
//        grupe = grupaViewModel.getGroupsByPredmet("AFJ")
//        MatcherAssert.assertThat(grupe, hasItem<Predmet>(hasProperty("naziv", Is("AFJ1")))
//        )
//        MatcherAssert.assertThat(grupe, hasItem<Predmet>(hasProperty("naziv", Is("AFJ2")))
//        )
//
//    }
//}