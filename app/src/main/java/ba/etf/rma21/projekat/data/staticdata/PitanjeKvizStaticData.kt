//
//package ba.etf.rma21.projekat.data.staticdata
//
//import ba.etf.rma21.projekat.data.models.PitanjeKviz
//
//fun allPitanjaKviz() : List<PitanjeKviz> {
//
//    return listOf(
//            // CCI - KVIZ 1
//            PitanjeKviz("Pitanje 1", "Kviz 1", "CCI", null),
//            PitanjeKviz("Pitanje 2", "Kviz 1", "CCI", null),
//            PitanjeKviz("Pitanje 3", "Kviz 1", "CCI", null),
//            PitanjeKviz("Pitanje 4", "Kviz 1", "CCI", null),
//
//            // CCI - KVIZ 2
//            PitanjeKviz("Pitanje 5", "Kviz 2", "CCI", null),
//            PitanjeKviz("Pitanje 6", "Kviz 2", "CCI", null),
//            PitanjeKviz("Pitanje 7", "Kviz 2", "CCI", null),
//            PitanjeKviz("Pitanje 8", "Kviz 2", "CCI", null),
//
//            // CCI - KVIZ 3
//            PitanjeKviz("Pitanje 9", "Kviz 3", "CCI", 1),
//            PitanjeKviz("Pitanje 10", "Kviz 3", "CCI", 2),
//            PitanjeKviz("Pitanje 11", "Kviz 3", "CCI", 1),
//            PitanjeKviz("Pitanje 12", "Kviz 3", "CCI", 2),
//
//            // AFJ - KVIZ 2
//            PitanjeKviz("Pitanje 13", "Kviz 2", "AFJ", null),
//            PitanjeKviz("Pitanje 14", "Kviz 2", "AFJ", null),
//            PitanjeKviz("Pitanje 15", "Kviz 2", "AFJ", null),
//            PitanjeKviz("Pitanje 16", "Kviz 2", "AFJ", null),
//
//            // RMA - KVIZ 2
//            PitanjeKviz("Pitanje 17", "Kviz 2", "RMA", null),
//            PitanjeKviz("Pitanje 18", "Kviz 2", "RMA", null),
//            PitanjeKviz("Pitanje 19", "Kviz 2", "RMA", null),
//            PitanjeKviz("Pitanje 20", "Kviz 2", "RMA", null),
//
//            // RMA - KVIZ 3
//            PitanjeKviz("Pitanje 21", "Kviz 3", "RMA", null),
//            PitanjeKviz("Pitanje 22", "Kviz 3", "RMA", null),
//            PitanjeKviz("Pitanje 23", "Kviz 3", "RMA", null),
//            PitanjeKviz("Pitanje 24", "Kviz 3", "RMA", null),
//
//            // OOAD - KVIZ 1
//            PitanjeKviz("Pitanje 25", "Kviz 1", "OOAD", null),
//            PitanjeKviz("Pitanje 26", "Kviz 1", "OOAD", null),
//            PitanjeKviz("Pitanje 27", "Kviz 1", "OOAD", null),
//            PitanjeKviz("Pitanje 28", "Kviz 1", "OOAD", null),
//
//            // TP - KVIZ 1
//            PitanjeKviz("Pitanje 29", "Kviz 1", "TP", null),
//            PitanjeKviz("Pitanje 30", "Kviz 1", "TP", null),
//            PitanjeKviz("Pitanje 31", "Kviz 1", "TP", null),
//            PitanjeKviz("Pitanje 32", "Kviz 1", "TP", null),
//
//            // OBP - KVIZ 1
//            PitanjeKviz("Pitanje 33", "Kviz 1", "OBP", null),
//            PitanjeKviz("Pitanje 34", "Kviz 1", "OBP", null),
//            PitanjeKviz("Pitanje 35", "Kviz 1", "OBP", null),
//            PitanjeKviz("Pitanje 36", "Kviz 1", "OBP", null),
//
//            // RA - KVIZ 1
//            PitanjeKviz("Pitanje 37", "Kviz 1", "RA", null),
//            PitanjeKviz("Pitanje 38", "Kviz 1", "RA", null),
//            PitanjeKviz("Pitanje 39", "Kviz 1", "RA", null),
//            PitanjeKviz("Pitanje 40", "Kviz 1", "RA", null),
//
//            // RA - KVIZ 2
//            PitanjeKviz("Pitanje 41", "Kviz 2", "RA", null),
//            PitanjeKviz("Pitanje 42", "Kviz 2", "RA", null),
//            PitanjeKviz("Pitanje 43", "Kviz 2", "RA", null),
//            PitanjeKviz("Pitanje 44", "Kviz 2", "RA", null),
//
//            // RPR - KVIZ 2
//            PitanjeKviz("Pitanje 45", "Kviz 2", "RPR", null),
//            PitanjeKviz("Pitanje 46", "Kviz 2", "RPR", null),
//            PitanjeKviz("Pitanje 47", "Kviz 2", "RPR", null),
//            PitanjeKviz("Pitanje 48", "Kviz 2", "RPR", null),
//
//            // DM - KVIZ 2
//            PitanjeKviz("Pitanje 49", "Kviz 2", "DM", null),
//            PitanjeKviz("Pitanje 50", "Kviz 2", "DM", null),
//            PitanjeKviz("Pitanje 51", "Kviz 2", "DM", null),
//            PitanjeKviz("Pitanje 52", "Kviz 2", "DM", null)
//
//    )
//
//}