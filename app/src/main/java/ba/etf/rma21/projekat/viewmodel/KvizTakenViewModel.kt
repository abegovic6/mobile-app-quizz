package ba.etf.rma21.projekat.viewmodel

import ba.etf.rma21.projekat.data.models.KvizTaken
import ba.etf.rma21.projekat.data.repositories.TakeKvizRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class KvizTakenViewModel {
    val scope = CoroutineScope(Job() + Dispatchers.IO)

    fun zapocniKviz(idKviza:Int, onSuccess: (kvizTaken: KvizTaken) -> Unit,
                   onError: () -> Unit) {
        scope.launch {
            val result = TakeKvizRepository.zapocniKviz(idKviza)
            when (result) {
                is KvizTaken -> onSuccess.invoke(result)
                else -> onError.invoke()
            }
        }
    }
}