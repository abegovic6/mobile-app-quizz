//package ba.etf.rma21.projekat.data.staticdata
//
//import ba.etf.rma21.projekat.data.models.Grupa
//
//fun allGroup() : List<Grupa> {
//    return listOf(
//            // RMA
//            Grupa("RMA1", "RMA"),
//            Grupa("RMA2", "RMA"),
//
//            // CCI
//            Grupa("CCI1", "CCI"),
//            Grupa("CCI2", "CCI"),
//
//            //OOAD
//            Grupa("OOAD1", "OOAD"),
//            Grupa("OOAD2", "OOAD"),
//
//            // TP
//            Grupa("TP1", "TP"),
//            Grupa("TP2", "TP"),
//
//            // OBP
//            Grupa("OBP1", "OBP"),
//            Grupa("OBP2", "OBP"),
//
//            // AFJ
//            Grupa("AFJ1", "AFJ"),
//            Grupa("AFJ2", "AFJ"),
//
//            // RA
//            Grupa("RA1", "RA"),
//            Grupa("RA2", "RA"),
//
//            // RPR
//            Grupa("RPR1", "RPR"),
//            Grupa("RPR2", "RPR"),
//
//            // DM
//            Grupa("DM1", "DM"),
//            Grupa("DM2", "DM")
//
//    )
//}