//package ba.etf.rma21.projekat
//
//import ba.etf.rma21.projekat.data.models.Grupa
//import ba.etf.rma21.projekat.data.models.Predmet
//import ba.etf.rma21.projekat.viewmodel.KorisnikViewModel
//import ba.etf.rma21.projekat.viewmodel.PredmetViewModel
//import org.hamcrest.CoreMatchers.`is` as Is
//import org.hamcrest.MatcherAssert
//import org.hamcrest.Matchers.*
//import org.junit.Assert
//import org.junit.Test
//
//class KorisnikUnitTest {
//    val korisnikViewModel = KorisnikViewModel()
//
//    @Test
//    fun getMyKvizoviTest() {
//        val kvizovi = korisnikViewModel.getMyKvizovi()
//
//        MatcherAssert.assertThat(kvizovi, hasItem<Predmet>(hasProperty("nazivPredmeta", Is("CCI")))
//        )
//        MatcherAssert.assertThat(kvizovi, hasItem<Predmet>(hasProperty("nazivPredmeta", Is("AFJ")))
//        )
//    }
//
//    @Test
//    fun dodavanjeTest() {
//        korisnikViewModel.addPredmet(Predmet("RMA", 1))
//        korisnikViewModel.addGrupa(Grupa("RMA1", "RMA"))
//        val kvizovi = korisnikViewModel.getMyKvizovi()
//
//        MatcherAssert.assertThat(kvizovi, hasItem<Predmet>(hasProperty("nazivPredmeta", Is("RMA")))
//        )
//        MatcherAssert.assertThat(kvizovi, hasItem<Predmet>(hasProperty("nazivGrupe", Is("RMA1")))
//        )
//    }
//
//}