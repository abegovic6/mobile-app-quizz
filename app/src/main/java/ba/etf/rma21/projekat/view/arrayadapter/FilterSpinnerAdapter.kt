package ba.etf.rma21.projekat.view.arrayadapter

import android.view.View
import android.widget.AdapterView

class FilterSpinnerAdapter(
        private val filtriranjeOnClick: (position : Int) -> Unit
) : AdapterView.OnItemSelectedListener {

    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        filtriranjeOnClick(position)
    }

    override fun onNothingSelected(parent: AdapterView<*>) {
        // write code to perform some action
    }

}