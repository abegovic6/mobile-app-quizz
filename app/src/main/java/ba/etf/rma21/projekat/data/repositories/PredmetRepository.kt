package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.models.Predmet
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class PredmetRepository {

    companion object {

        fun setContext(_context: Context){
            DBRepository.setContext(_context)
        }

        suspend fun getUpisaniApi(): List<Predmet> {
            return withContext(Dispatchers.IO) {
                val result = mutableListOf<Predmet>()
                val upisaneGrupe = ApiAdapter.retrofit.getGrupeZaStudenta(AccountRepository.getHash())
                for (grupa in upisaneGrupe) {
                    val predmet = ApiAdapter.retrofit.getPredmetSaId(grupa.predmetId)
                    var sadrzi = false
                    for(p in result) {
                        if(p.id == predmet.id)
                            sadrzi = true
                    }
                    if(!sadrzi) result.add(predmet)
                }
                return@withContext result
            }
        }

        suspend fun getUpisani(): List<Predmet> {
            return withContext(Dispatchers.IO) {
                return@withContext DBRepository.getUpisanePredmete()
            }
        }

        suspend fun getAll(): List<Predmet> {
            return withContext(Dispatchers.IO) {
                return@withContext ApiAdapter.retrofit.getAllPredmeti()
            }
        }

        suspend fun getNeupisaniPoGodini(godina : Int): List<Predmet> {
            val predmeti = mutableListOf<Predmet>()
            for(svi in getAll()) {
                if(godina == svi.godina && !getUpisani().contains(svi))
                    predmeti.add(svi)
            }
            return predmeti
        }
    }

}