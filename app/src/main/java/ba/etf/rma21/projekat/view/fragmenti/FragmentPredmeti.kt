package ba.etf.rma21.projekat.view.fragmenti

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.MainActivity
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Message
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.view.arrayadapter.GodinaSpinnerAdapter
import ba.etf.rma21.projekat.view.arrayadapter.GrupaSpinnerAdapter
import ba.etf.rma21.projekat.view.arrayadapter.PredmetSpinnerAdapter
import ba.etf.rma21.projekat.viewmodel.GrupaViewModel
import ba.etf.rma21.projekat.viewmodel.KorisnikViewModel
import ba.etf.rma21.projekat.viewmodel.PredmetViewModel
import ba.etf.rma21.projekat.viewmodel.KvizPredmetViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class FragmentPredmeti : Fragment(){
    private lateinit var dodajPredmetDugme : Button
    private lateinit var odabirGodina : Spinner
    private lateinit var odabirPredmet : Spinner
    private lateinit var odabirGrupa : Spinner
    private val korisnikViewModel = KorisnikViewModel()

    private var brojac = 0 // potreban za spremanje predmeta

    private var predmetViewModel = PredmetViewModel()
    private var grupaViewModel = GrupaViewModel()

    private lateinit var viewModel: KvizPredmetViewModel

    private var grupeGlavna = listOf<Grupa>()
    private var predmetiGlavna = listOf<Predmet>()
    private var idGrupe = 0


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_predmeti, container, false)

        odabirGodina = view.findViewById(R.id.odabirGodina)
        odabirPredmet = view.findViewById(R.id.odabirPredmet)
        odabirGrupa = view.findViewById(R.id.odabirGrupa)
        dodajPredmetDugme = view.findViewById(R.id.dodajPredmetDugme)

        viewModel = (activity as MainActivity).kvizPredmetViewModel

        requireActivity().onBackPressedDispatcher.addCallback(this,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    val fragmentKvizovi = FragmentKvizovi.newInstance()
                    replaceFragment(fragmentKvizovi)
                }
            })


        setUpDodajPredmetDugme()
        setUpOdabirGodina()

        return view
    }

    fun onSuccessPredmet(predmeti:List<Predmet>){
        GlobalScope.launch(Dispatchers.IO){
            withContext(Dispatchers.Main){
                predmetiGlavna = predmeti
                setUpOdabirPredmet()
            }
        }
    }
    fun onSuccessGrupa(grupe:List<Grupa>){
        GlobalScope.launch(Dispatchers.IO){
            withContext(Dispatchers.Main){
                grupeGlavna = grupe
                setUpOdabirGrupa()
            }
        }
    }
    fun onSuccessUpis(string: Message){
        GlobalScope.launch(Dispatchers.IO){
            withContext(Dispatchers.Main){
                println(string)
            }
        }
    }
    fun onSuccessIDGrupe(id: Int){
        GlobalScope.launch(Dispatchers.IO){
            withContext(Dispatchers.Main){
                idGrupe = id
            }
        }
    }
    fun onError() {
        val toast = Toast.makeText(context, "Search error", Toast.LENGTH_SHORT)
        toast.show()
    }

    companion object {
        fun newInstance(): FragmentPredmeti = FragmentPredmeti()
    }

    private fun setUpOdabirGodina () {
        val godine = resources.getStringArray(R.array.Godine)
        val adapter= ArrayAdapter(odabirGodina.context, R.layout.item_spinner, godine)
        odabirGodina.adapter=adapter
        if(brojac == 0)
            odabirGodina.setSelection(viewModel.godina)
        odabirGodina.onItemSelectedListener =
                GodinaSpinnerAdapter(
                        funckijaOnClick = {godina -> setUpOdabirPredmet(godina)}
                )
    }

    private fun setUpOdabirPredmet (godina : Int) {
        predmetViewModel.getNeupisaniPoGodini(
            godina = godina,
            onSuccess = ::onSuccessPredmet,
            onError = ::onError
        )
    }

    private fun setUpOdabirPredmet () {
        var predmeti = mutableListOf<String>()
        predmeti = predmetiGlavna.map { it -> it.naziv }.toMutableList()
        predmeti.add(0, "")
        val adapter= ArrayAdapter(odabirGodina.context, R.layout.item_spinner, predmeti)
        brojac++
        odabirPredmet.adapter=adapter
        viewModel.godina(odabirGodina.selectedItemPosition)
        odabirPredmet.onItemSelectedListener =
                PredmetSpinnerAdapter(
                        predmeti = predmeti,
                        funckijaOnClick = { predmet -> setUpOdabirGrupa(predmet)}
                )
        if(brojac == 1) {
            odabirPredmet.setSelection(viewModel.predmet)
            viewModel.predmet(0)
        }
    }

    private fun setUpOdabirGrupa (predmet : String?) {
        grupaViewModel.getGroupsByPredmet(
            nazivPredmet = predmet!!,
            onSuccess = ::onSuccessGrupa,
            onError = ::onError
        )
    }

    private fun setUpOdabirGrupa () {
        var grupe = mutableListOf<String>()
        grupe = grupeGlavna.map { it -> it.naziv }.toMutableList()
        grupe.add(0, "")
        val adapter= ArrayAdapter(odabirGodina.context, R.layout.item_spinner, grupe)
        odabirGrupa.adapter=adapter
        brojac++
        viewModel.predmet(odabirPredmet.selectedItemPosition)
        odabirGrupa.onItemSelectedListener =
                GrupaSpinnerAdapter(
                        funckijaOnClick = { dugmeEditable()}
                )
        if(brojac == 2) {
            odabirGrupa.setSelection(viewModel.grupa)
            viewModel.grupa(0)
        }
    }

    private fun dugmeEditable() {
        viewModel.grupa(odabirGrupa.selectedItemPosition)
        if(odabirPredmet.selectedItem.toString() == "" ) {
            dodajPredmetDugme.isEnabled = false
        } else {
            dodajPredmetDugme.isEnabled = odabirGrupa.selectedItem.toString() != ""
        }
    }

    private fun setUpDodajPredmetDugme () {
        dodajPredmetDugme.setOnClickListener {
            viewModel.grupa(0)
            viewModel.predmet(0)

            idGrupe = grupeGlavna[odabirGrupa.selectedItemPosition - 1].id

            grupaViewModel.upisiUGrupu(
                    idGrupe = idGrupe,
                    onSuccess = ::onSuccessUpis,
                    onError = ::onError)


            val bundle = Bundle().apply {
                putString("tekst", "Uspješno ste upisani u grupu ${odabirGrupa.selectedItem} predmeta ${odabirPredmet.selectedItem}!")
            }

            val fragment = FragmentPoruka.newInstance()
            fragment.arguments = bundle
            replaceFragment(fragment)
        }
    }

    private fun replaceFragment(fragment: Fragment) {
        val transaction = fragmentManager?.beginTransaction()
        if (transaction != null) {
            transaction.replace(R.id.container, fragment)
            transaction.addToBackStack(null)
            transaction.commit()
        }
    }


}