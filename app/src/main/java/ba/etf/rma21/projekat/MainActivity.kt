package ba.etf.rma21.projekat

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.data.repositories.AccountRepository
import ba.etf.rma21.projekat.view.fragmenti.FragmentKvizovi
import ba.etf.rma21.projekat.view.fragmenti.FragmentPredmeti
import ba.etf.rma21.projekat.viewmodel.AccountViewModel
import ba.etf.rma21.projekat.viewmodel.DBViewModel
import ba.etf.rma21.projekat.viewmodel.KvizPredmetViewModel
import ba.etf.rma21.projekat.viewmodel.PitanjePokusajViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class MainActivity : AppCompatActivity() {
    private lateinit var bottomNavigation: BottomNavigationView
    private var pitanjePokusajViewModel = PitanjePokusajViewModel()
    val accountViewModel = AccountViewModel()
    val kvizPredmetViewModel = KvizPredmetViewModel()

    fun getBottomNavigation() : BottomNavigationView {
        return bottomNavigation
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val uri = intent
        if (uri != null) {
            val hash = uri.getStringExtra("payload")
            if (hash != null) {
                this?.let { accountViewModel.postaviHash(it, hash, onSuccess = ::onSuccess, onError = ::onError)}
                val toast = Toast.makeText(this, "Trenutni hash korisnika: " + hash, Toast.LENGTH_SHORT)
                toast.show()
            }

        }
        else {
            this?.let { accountViewModel.postaviHash(it, AccountRepository.acHash, onSuccess = ::onSuccess, onError = ::onError)}
            val toast = Toast.makeText(this, "Trenutni hash korisnika: " + AccountRepository.acHash, Toast.LENGTH_SHORT)
            toast.show()
        }

        bottomNavigation= findViewById(R.id.bottomNav)

        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        bottomNavigation.selectedItemId= R.id.kvizovi
        val fragmentKvizovi = FragmentKvizovi.newInstance()
        openFragment(fragmentKvizovi, "kvizovi")
    }

    fun onSuccess(hash: Boolean){
        GlobalScope.launch(Dispatchers.IO){
            withContext(Dispatchers.Main){

            }
        }
    }
    fun onError() {

    }


    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.kvizovi -> {
                val fragmentKvizovi = FragmentKvizovi.newInstance()
                openFragment(fragmentKvizovi, "kvizovi")
                pitanjePokusajViewModel.boja("#FFFFFF")
                return@OnNavigationItemSelectedListener true
            }
            R.id.predmeti -> {
                val fragmentPredmeti = FragmentPredmeti.newInstance()
                openFragment(fragmentPredmeti, "predmeti")
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }


    private fun openFragment(fragment: Fragment, backStackString: String) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(backStackString)
        transaction.commit()
    }


}

