package ba.etf.rma21.projekat.view.arrayadapter

import android.view.View
import android.widget.AdapterView

class GodinaSpinnerAdapter (
    private val funckijaOnClick: (position : Int) -> Unit) : AdapterView.OnItemSelectedListener {

    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        funckijaOnClick(position + 1)

    }

    override fun onNothingSelected(parent: AdapterView<*>) {
        // write code to perform some action
    }


}