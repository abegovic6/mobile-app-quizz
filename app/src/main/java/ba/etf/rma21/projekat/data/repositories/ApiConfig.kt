package ba.etf.rma21.projekat.data.repositories

class ApiConfig {
    companion object {
        var baseURL = "https://rma21-etf.herokuapp.com"

        fun postaviBaseURL (url:String) {
            baseURL = url
        }

        fun getBaseUrl() : String {
            return baseURL
        }
    }

}