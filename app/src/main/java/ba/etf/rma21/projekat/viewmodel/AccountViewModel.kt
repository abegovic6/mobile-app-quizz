package ba.etf.rma21.projekat.viewmodel

import android.content.Context
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.repositories.AccountRepository
import ba.etf.rma21.projekat.data.repositories.DBRepository
import ba.etf.rma21.projekat.data.repositories.PitanjeKvizRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class AccountViewModel() {
    val scope = CoroutineScope(Job() + Dispatchers.IO)

    fun postaviHash(context: Context, payload: String, onSuccess: (postavljeno: Boolean) -> Unit,
                    onError: () -> Unit) {
        scope.launch {
            AccountRepository.setContext(context)
            val result = AccountRepository.postaviHash(payload)
            DBRepository.updateNow()
            when (result) {
                is Boolean -> onSuccess.invoke(result)
                else -> onError.invoke()
            }
        }
    }

//    fun obrisiSveZaKorisnika(onSuccess: (postavljeno: Boolean) -> Unit,
//                             onError: () -> Unit) {
//        scope.launch {
//            val result = AccountRepository.postaviHash(payload)
//            when (result) {
//                is Boolean -> onSuccess.invoke(result)
//                else -> onError.invoke()
//            }
//        }
//    }

}