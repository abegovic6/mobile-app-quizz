package ba.etf.rma21.projekat.viewmodel

class KvizPredmetViewModel {
    private var mutableGodina = 0
    private var mutableGrupa = 0
    private var mutablePredmet = 0

    val godina: Int get() = mutableGodina
    val predmet: Int get() = mutablePredmet
    val grupa: Int get() = mutableGrupa

    fun godina(item: Int) {
        mutableGodina = item
    }

    fun predmet(item: Int) {
        mutablePredmet = item
    }

    fun grupa(item: Int) {
        mutableGrupa = item
    }
}