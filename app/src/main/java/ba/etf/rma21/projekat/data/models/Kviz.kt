package ba.etf.rma21.projekat.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*

@Entity
data class Kviz(
        @PrimaryKey @SerializedName("id") val id: Int?,
        @ColumnInfo(name = "naziv") @SerializedName("naziv") val naziv: String,
        @ColumnInfo(name = "nazivPredmeta")var nazivPredmeta: String?,
        @ColumnInfo(name = "datumPocetka") @SerializedName("datumPocetak") val datumPocetka: String,
        @ColumnInfo(name = "datumKraj") @SerializedName("datumKraj") val datumKraj: String?,
        @ColumnInfo(name = "datumRada") var datumRada: String?,
        @ColumnInfo(name = "trajanje") @SerializedName("trajanje") val trajanje: Int,
        @ColumnInfo(name = "nazivGrupe") var nazivGrupe: String?,
        @ColumnInfo(name = "odvojeniBodovi") var osvojeniBodovi: Float?,
        @ColumnInfo(name = "predan") var predan: Boolean = false
): Serializable