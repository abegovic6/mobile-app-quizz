package ba.etf.rma21.projekat.view.arrayadapter

import android.view.View
import android.widget.AdapterView

class PredmetSpinnerAdapter(
        var predmeti: List<String>,
        private val funckijaOnClick: (predmet : String) -> Unit)  : AdapterView.OnItemSelectedListener {

    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        funckijaOnClick(predmeti[position]!!)
    }

    override fun onNothingSelected(parent: AdapterView<*>) {
        // write code to perform some action
    }

}