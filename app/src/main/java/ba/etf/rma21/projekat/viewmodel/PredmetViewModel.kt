package ba.etf.rma21.projekat.viewmodel

import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.data.repositories.PredmetRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class PredmetViewModel {
    val scope = CoroutineScope(Job() + Dispatchers.IO)

    fun getUpisani(onSuccess: (kvizovi: List<Predmet>) -> Unit,
                   onError: () -> Unit) {
        scope.launch {
            val result = PredmetRepository.getUpisaniApi()
            when (result) {
                is List<Predmet> -> onSuccess.invoke(result)
                else -> onError.invoke()
            }
        }
    }

    fun getAll(onSuccess: (kvizovi: List<Predmet>) -> Unit,
               onError: () -> Unit) {
        scope.launch {
            val result = PredmetRepository.getAll()
            when (result) {
                is List<Predmet> -> onSuccess.invoke(result)
                else -> onError.invoke()
            }
        }
    }

    fun getNeupisaniPoGodini(godina: Int,onSuccess: (kvizovi: List<Predmet>) -> Unit,
                             onError: () -> Unit) {
        scope.launch {
            val result = PredmetRepository.getNeupisaniPoGodini(godina)
            when (result) {
                is List<Predmet> -> onSuccess.invoke(result)
                else -> onError.invoke()
            }
        }
    }
}