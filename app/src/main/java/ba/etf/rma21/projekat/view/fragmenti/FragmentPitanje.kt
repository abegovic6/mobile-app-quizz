package ba.etf.rma21.projekat.view.fragmenti

import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.view.children
import androidx.lifecycle.ViewModelProviders
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.KvizTaken
import ba.etf.rma21.projekat.data.models.Odgovor
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.view.arrayadapter.OdgovoriListAdapter
import ba.etf.rma21.projekat.viewmodel.PitanjaKvizViewModel
import ba.etf.rma21.projekat.viewmodel.PitanjePokusajViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class FragmentPitanje(var pitanje : Pitanje) : Fragment() {
    private lateinit var tekstPitanja : TextView
    private lateinit var odgovoriLista : ListView

    private val pitanjaKvizViewModel = PitanjaKvizViewModel()
    private lateinit var pitanjePokusajViewModel: PitanjePokusajViewModel

    private var odgovor : Odgovor? = null
    private var kviz: Kviz? = null
    private var kvizTaken: KvizTaken? = null

    private var odgovoreno = false



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_pitanje, container, false)

        requireActivity().onBackPressedDispatcher.addCallback(this,
                object : OnBackPressedCallback(true) {
                    override fun handleOnBackPressed() {

                    }
                })

        tekstPitanja = view.findViewById(R.id.tekstPitanja)
        odgovoriLista = view.findViewById(R.id.odgovoriLista)

        pitanjePokusajViewModel = activity?.run {
            ViewModelProviders.of(this)[PitanjePokusajViewModel::class.java]
        } ?: throw Exception("Invalid Activity")

        val bundle = this.arguments
        var enabled  = true
        if(bundle != null) {
            enabled = bundle.getBoolean("enabled")
            kviz = bundle.getSerializable("kviz") as Kviz
            kvizTaken = bundle.getSerializable("kvizTaken") as KvizTaken
        }

        pitanjaKvizViewModel.getOdgovor(idKviza = kviz!!.id!!, idPitanja = pitanje.id!!, onSuccess = ::onSuccess, onError = ::onError, context = context!!)



        setUpTekstPitanja()
        setUpOdgovoriLista(inflater)

        return view
    }

    fun onSuccess(odg: Odgovor?){
        GlobalScope.launch(Dispatchers.IO){
            withContext(Dispatchers.Main){
                odgovor = odg
                Handler().postDelayed({
                    if(odgovor != null && odgovor!!.odgovoreno != pitanje.opcije.size){
                        odgovoriLista.performItemClick(odgovoriLista, odgovor!!.odgovoreno, odgovoriLista.adapter.getItemId(odgovor!!.odgovoreno))
                    }
                }, 1)

                if(odgovor != null) {
                    for(item in odgovoriLista.children) {
                        item.isEnabled = false
                        item.setOnClickListener(null)
                    }
                }
            }
        }
    }
    fun onError() {
        val toast = Toast.makeText(context, "Search error", Toast.LENGTH_SHORT)
        toast.show()
    }

    private fun setUpTekstPitanja() {
        tekstPitanja.text = pitanje.tekstPitanja
    }

    private fun setUpOdgovoriLista(inflater: LayoutInflater) {
        val adapter = ArrayAdapter<String>(inflater.context, R.layout.item_list, pitanje.opcije)
        odgovoriLista.adapter = adapter
        odgovoriLista.onItemClickListener = OdgovoriListAdapter(pitanje, pitanjePokusajViewModel) {
            pozicija -> funkcijaOnClick(pozicija)
        }
    }

    fun onSuccessPostavi(bodovi: Int){
        GlobalScope.launch(Dispatchers.IO){
            withContext(Dispatchers.Main){
                pitanjePokusajViewModel.bodovi(bodovi)
            }
        }
    }

    private fun funkcijaOnClick(position : Int) {
        pitanjaKvizViewModel.postaviOdgovorKviz(idKviz =  pitanjePokusajViewModel.id,
                idPitanje = pitanje.id!!,
                odgovor = position,
                onSuccess = ::onSuccessPostavi,
                onError = ::onError, context = context!!)
    }

    companion object {
        fun newInstance(pitanje: Pitanje): FragmentPitanje = FragmentPitanje(pitanje)
    }
}