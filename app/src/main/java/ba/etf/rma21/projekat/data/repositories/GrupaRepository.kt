package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Message
import ba.etf.rma21.projekat.data.models.Predmet
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class GrupaRepository {
    companion object {

        fun setContext(_context: Context){
            DBRepository.setContext(_context)
        }

        suspend fun getGroupsByPredmet(nazivPredmet: String): List<Grupa> {
            return withContext(Dispatchers.IO) {
                val predmeti = ApiAdapter.retrofit.getAllPredmeti()
                var idPredmeta = 0
                for(predmet in predmeti) {
                    if (predmet.naziv == nazivPredmet)
                        idPredmeta = predmet.id!!
                }
                if(idPredmeta == 0) return@withContext listOf<Grupa>()
                return@withContext ApiAdapter.retrofit.getGrupaZaPredmet(idPredmeta)
            }
        }

        suspend fun upisiUGrupu(idGrupe: Int) : Message{
            return withContext(Dispatchers.IO) {
                return@withContext ApiAdapter.retrofit.upisiUGrupu(idGrupe, AccountRepository.getHash())
            }
        }

        suspend fun grupe() : List<Grupa> {
            return withContext(Dispatchers.IO) {
                return@withContext ApiAdapter.retrofit.getAllGroups()
            }
        }

        suspend fun getIdGrupeSaNazivom (naziv: String) : Int? {
            return withContext(Dispatchers.IO) {
                val grupe = grupe()
                return@withContext grupe.find { it -> it.naziv == naziv }?.id
            }
        }

    }
}