//package ba.etf.rma21.projekat
//
//import ba.etf.rma21.projekat.data.models.Predmet
//import ba.etf.rma21.projekat.viewmodel.PredmetViewModel
//import org.hamcrest.CoreMatchers.`is` as Is
//import org.hamcrest.MatcherAssert
//import org.hamcrest.Matchers.*
//import org.junit.Assert
//import org.junit.Test
//
//class PredmetUnitTest {
//    val predmetViewModel = PredmetViewModel()
//
//    @Test
//    fun getUpisaniTest() {
//        val predmeti = predmetViewModel.getUpisani()
//
//        MatcherAssert.assertThat(predmeti, hasItem<Predmet>(hasProperty("naziv", Is("CCI")))
//        )
//        MatcherAssert.assertThat(predmeti, hasItem<Predmet>(hasProperty("naziv", Is("AFJ")))
//        )
//    }
//
//    @Test
//    fun getAllTest() {
//        val predmeti = predmetViewModel.getAll()
//
//        MatcherAssert.assertThat(predmeti, hasItem<Predmet>(hasProperty("naziv", Is("RMA")))
//        )
//        MatcherAssert.assertThat(predmeti, hasItem<Predmet>(hasProperty("naziv", Is("CCI")))
//        )
//        MatcherAssert.assertThat(predmeti, hasItem<Predmet>(hasProperty("naziv", Is("OOAD")))
//        )
//        MatcherAssert.assertThat(predmeti, hasItem<Predmet>(hasProperty("naziv", Is("TP")))
//        )
//        MatcherAssert.assertThat(predmeti, hasItem<Predmet>(hasProperty("naziv", Is("OBP")))
//        )
//    }
//
//    @Test
//    fun getNeupisaniPoGodiniTest() {
//        var predmeti = predmetViewModel.getNeupisaniPoGodini(1)
//        MatcherAssert.assertThat(predmeti, hasItem<Predmet>(hasProperty("naziv", Is("RMA")))
//        )
//
//        predmeti = predmetViewModel.getNeupisaniPoGodini(2)
//        MatcherAssert.assertThat(predmeti, hasItem<Predmet>(hasProperty("naziv", Is("OOAD")))
//        )
//        MatcherAssert.assertThat(predmeti, hasItem<Predmet>(hasProperty("naziv", Is("TP")))
//        )
//
//        predmeti = predmetViewModel.getNeupisaniPoGodini(3)
//        MatcherAssert.assertThat(predmeti, hasItem<Predmet>(hasProperty("naziv", Is("OBP")))
//        )
//    }
//
//
//
//}