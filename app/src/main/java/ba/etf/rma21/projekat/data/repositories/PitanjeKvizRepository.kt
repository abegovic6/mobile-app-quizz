package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.models.PitanjeKviz
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class PitanjeKvizRepository {

    companion object {
        fun setContext(_context: Context){
            DBRepository.setContext(_context)
        }


        suspend fun getPitanjaApi(idKviza: Int?):List<Pitanje>? {
            return withContext(Dispatchers.IO) {
                return@withContext ApiAdapter.retrofit.getPitanjaForQuiz(idKviza!!)
            }
        }

        suspend fun getPitanja(idKviza: Int?):List<Pitanje>? {
            return withContext(Dispatchers.IO) {
                return@withContext DBRepository.getPitanjaZaKviz(idKviza!!)
            }
        }

    }

}