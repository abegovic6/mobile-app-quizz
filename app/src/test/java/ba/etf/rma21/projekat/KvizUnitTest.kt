//package ba.etf.rma21.projekat
//
//import ba.etf.rma21.projekat.data.models.Predmet
//import ba.etf.rma21.projekat.viewmodel.KvizViewModel
//import org.hamcrest.MatcherAssert
//import org.hamcrest.CoreMatchers.`is` as Is
//import org.hamcrest.Matchers.*
//import org.junit.Test
//
//class KvizUnitTest {
//    val kvizViewModel = KvizViewModel()
//
//    @Test
//    fun getMyKvizesTest() {
//        val kvizovi = kvizViewModel.getMyKvizes()
//
//        MatcherAssert.assertThat(kvizovi, hasItem<Predmet>(hasProperty("nazivPredmeta", Is("CCI")))
//        )
//        MatcherAssert.assertThat(kvizovi, hasItem<Predmet>(hasProperty("nazivPredmeta", Is("AFJ")))
//        )
//    }
//
//    @Test
//    fun getAllTest() {
//        val kvizovi = kvizViewModel.getAll()
//
//        MatcherAssert.assertThat(kvizovi, hasItem<Predmet>(hasProperty("nazivPredmeta", Is("DM")))
//        )
//    }
//
//    @Test
//    fun getDoneTest() {
//        val kvizovi = kvizViewModel.getDone()
//
//        MatcherAssert.assertThat(kvizovi, hasItem<Predmet>(hasProperty("nazivPredmeta", Is("CCI")))
//        )
//        MatcherAssert.assertThat(kvizovi, hasItem<Predmet>(hasProperty("naziv", Is("Kviz 1")))
//        )
//    }
//
//    @Test
//    fun getFutureTest() {
//        val kvizovi = kvizViewModel.getFuture()
//
//        MatcherAssert.assertThat(kvizovi, hasItem<Predmet>(hasProperty("nazivPredmeta", Is("AFJ")))
//        )
//        MatcherAssert.assertThat(kvizovi, hasItem<Predmet>(hasProperty("naziv", Is("Kviz 1")))
//        )
//    }
//
//    @Test
//    fun getNotTakenTest() {
//        val kvizovi = kvizViewModel.getNotTaken()
//
//        MatcherAssert.assertThat(kvizovi, hasItem<Predmet>(hasProperty("nazivPredmeta", Is("CCI")))
//        )
//        MatcherAssert.assertThat(kvizovi, hasItem<Predmet>(hasProperty("naziv", Is("Kviz 2")))
//        )
//    }
//}