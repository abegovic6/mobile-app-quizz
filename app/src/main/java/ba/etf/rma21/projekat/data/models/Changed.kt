package ba.etf.rma21.projekat.data.models

import com.google.gson.annotations.SerializedName

data class Changed(
    @SerializedName("changed") var changed: Boolean?,
    @SerializedName("message") var message: String?
)