package ba.etf.rma21.projekat.viewmodel

import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.Message
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.data.repositories.GrupaRepository
import ba.etf.rma21.projekat.data.repositories.KvizRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class GrupaViewModel {
    val scope = CoroutineScope(Job() + Dispatchers.IO)

    fun getGroupsByPredmet(nazivPredmet: String,onSuccess: (kvizovi: List<Grupa>) -> Unit,
                           onError: () -> Unit) {
        scope.launch {
            val result = GrupaRepository.getGroupsByPredmet(nazivPredmet)
            when (result) {
                is List<Grupa> -> onSuccess.invoke(result)
                else -> onError.invoke()
            }
        }
    }

    fun upisiUGrupu(idGrupe: Int, onSuccess: (povratnaPoruka: Message) -> Unit,
                           onError: () -> Unit) {
        scope.launch {
            val result = GrupaRepository.upisiUGrupu(idGrupe)
            when (result) {
                is Message -> onSuccess.invoke(result)
                else -> onError.invoke()
            }
        }
    }

    fun getIdGrupeSaNazivom(naziv: String, onSuccess: (povratnaPoruka: Int) -> Unit,
                    onError: () -> Unit) {
        scope.launch {
            val result = GrupaRepository.getIdGrupeSaNazivom(naziv)
            when (result) {
                is Int -> onSuccess.invoke(result)
                else -> onError.invoke()
            }
        }
    }


}