//package ba.etf.rma21.projekat.data.staticdata
//
//import ba.etf.rma21.projekat.data.models.Pitanje
//
//fun allPitanja() : List<Pitanje> {
//    return listOf(
//            Pitanje("Pitanje 1", "1. Izbaci uljeza:", listOf("NETAČNO", "NETAČNO", "NETAČNO", "TAČNO"), 4-1),
//            Pitanje("Pitanje 2", "2. Izbaci uljeza:", listOf("NETAČNO", "NETAČNO", "TAČNO", "NETAČNO"), 3-1),
//            Pitanje("Pitanje 3", "3. Izbaci uljeza:", listOf("NETAČNO", "TAČNO", "NETAČNO", "NETAČNO"), 2-1),
//            Pitanje("Pitanje 4", "4. Izbaci uljeza:", listOf("TAČNO", "NETAČNO", "NETAČNO", "NETAČNO"), 1-1),
//
//            Pitanje("Pitanje 5", "1. Izbaci uljeza:", listOf("NETAČNO", "NETAČNO", "NETAČNO", "TAČNO"), 4-1),
//            Pitanje("Pitanje 6", "2. Izbaci uljeza:", listOf("NETAČNO", "NETAČNO", "TAČNO", "NETAČNO"), 3-1),
//            Pitanje("Pitanje 7", "3. Izbaci uljeza:", listOf("NETAČNO", "TAČNO", "NETAČNO", "NETAČNO"), 2-1),
//            Pitanje("Pitanje 8", "4. Izbaci uljeza:", listOf("TAČNO", "NETAČNO", "NETAČNO", "NETAČNO"), 1-1),
//
//            Pitanje("Pitanje 9", "1. Izbaci uljeza:", listOf("NETAČNO", "NETAČNO", "NETAČNO", "TAČNO"), 4-1),
//            Pitanje("Pitanje 10", "2. Izbaci uljeza:", listOf("NETAČNO", "NETAČNO", "TAČNO", "NETAČNO"), 3-1),
//            Pitanje("Pitanje 11", "3. Izbaci uljeza:", listOf("NETAČNO", "TAČNO", "NETAČNO", "NETAČNO"), 2-1),
//            Pitanje("Pitanje 12", "4. Izbaci uljeza:", listOf("TAČNO", "NETAČNO", "NETAČNO", "NETAČNO"), 1-1),
//
//            Pitanje("Pitanje 13", "1. Izbaci uljeza:", listOf("NETAČNO", "NETAČNO", "NETAČNO", "TAČNO"), 4-1),
//            Pitanje("Pitanje 14", "2. Izbaci uljeza:", listOf("NETAČNO", "NETAČNO", "TAČNO", "NETAČNO"), 3-1),
//            Pitanje("Pitanje 15", "3. Izbaci uljeza:", listOf("NETAČNO", "TAČNO", "NETAČNO", "NETAČNO"), 2-1),
//            Pitanje("Pitanje 16", "4. Izbaci uljeza:", listOf("TAČNO", "NETAČNO", "NETAČNO", "NETAČNO"), 1-1),
//
//            Pitanje("Pitanje 17", "1. Izbaci uljeza:", listOf("NETAČNO", "NETAČNO", "NETAČNO", "TAČNO"), 4-1),
//            Pitanje("Pitanje 18", "2. Izbaci uljeza:", listOf("NETAČNO", "NETAČNO", "TAČNO", "NETAČNO"), 3-1),
//            Pitanje("Pitanje 19", "3. Izbaci uljeza:", listOf("NETAČNO", "TAČNO", "NETAČNO", "NETAČNO"), 2-1),
//            Pitanje("Pitanje 20", "4. Izbaci uljeza:", listOf("TAČNO", "NETAČNO", "NETAČNO", "NETAČNO"), 1-1),
//
//            Pitanje("Pitanje 21", "1. Izbaci uljeza:", listOf("NETAČNO", "NETAČNO", "NETAČNO", "TAČNO"), 4-1),
//            Pitanje("Pitanje 22", "2. Izbaci uljeza:", listOf("NETAČNO", "NETAČNO", "TAČNO", "NETAČNO"), 3-1),
//            Pitanje("Pitanje 23", "3. Izbaci uljeza:", listOf("NETAČNO", "TAČNO", "NETAČNO", "NETAČNO"), 2-1),
//            Pitanje("Pitanje 24", "4. Izbaci uljeza:", listOf("TAČNO", "NETAČNO", "NETAČNO", "NETAČNO"), 1-1),
//
//            Pitanje("Pitanje 25", "1. Izbaci uljeza:", listOf("NETAČNO", "NETAČNO", "NETAČNO", "TAČNO"), 4-1),
//            Pitanje("Pitanje 26", "2. Izbaci uljeza:", listOf("NETAČNO", "NETAČNO", "TAČNO", "NETAČNO"), 3-1),
//            Pitanje("Pitanje 27", "3. Izbaci uljeza:", listOf("NETAČNO", "TAČNO", "NETAČNO", "NETAČNO"), 2-1),
//            Pitanje("Pitanje 28", "4. Izbaci uljeza:", listOf("TAČNO", "NETAČNO", "NETAČNO", "NETAČNO"), 1-1),
//
//            Pitanje("Pitanje 29", "1. Izbaci uljeza:", listOf("NETAČNO", "NETAČNO", "NETAČNO", "TAČNO"), 4-1),
//            Pitanje("Pitanje 30", "2. Izbaci uljeza:", listOf("NETAČNO", "NETAČNO", "TAČNO", "NETAČNO"), 3-1),
//            Pitanje("Pitanje 31", "3. Izbaci uljeza:", listOf("NETAČNO", "TAČNO", "NETAČNO", "NETAČNO"), 2-1),
//            Pitanje("Pitanje 32", "4. Izbaci uljeza:", listOf("TAČNO", "NETAČNO", "NETAČNO", "NETAČNO"), 1-1),
//
//            Pitanje("Pitanje 33", "1. Izbaci uljeza:", listOf("NETAČNO", "NETAČNO", "NETAČNO", "TAČNO"), 4-1),
//            Pitanje("Pitanje 34", "2. Izbaci uljeza:", listOf("NETAČNO", "NETAČNO", "TAČNO", "NETAČNO"), 3-1),
//            Pitanje("Pitanje 35", "3. Izbaci uljeza:", listOf("NETAČNO", "TAČNO", "NETAČNO", "NETAČNO"), 2-1),
//            Pitanje("Pitanje 36", "4. Izbaci uljeza:", listOf("TAČNO", "NETAČNO", "NETAČNO", "NETAČNO"), 1-1),
//
//            Pitanje("Pitanje 37", "1. Izbaci uljeza:", listOf("NETAČNO", "NETAČNO", "NETAČNO", "TAČNO"), 4-1),
//            Pitanje("Pitanje 38", "2. Izbaci uljeza:", listOf("NETAČNO", "NETAČNO", "TAČNO", "NETAČNO"), 3-1),
//            Pitanje("Pitanje 39", "3. Izbaci uljeza:", listOf("NETAČNO", "TAČNO", "NETAČNO", "NETAČNO"), 2-1),
//            Pitanje("Pitanje 40", "4. Izbaci uljeza:", listOf("TAČNO", "NETAČNO", "NETAČNO", "NETAČNO"), 1-1),
//
//            Pitanje("Pitanje 41", "1. Izbaci uljeza:", listOf("NETAČNO", "NETAČNO", "NETAČNO", "TAČNO"), 4-1),
//            Pitanje("Pitanje 42", "2. Izbaci uljeza:", listOf("NETAČNO", "NETAČNO", "TAČNO", "NETAČNO"), 3-1),
//            Pitanje("Pitanje 43", "3. Izbaci uljeza:", listOf("NETAČNO", "TAČNO", "NETAČNO", "NETAČNO"), 2-1),
//            Pitanje("Pitanje 44", "4. Izbaci uljeza:", listOf("TAČNO", "NETAČNO", "NETAČNO", "NETAČNO"), 1-1),
//
//            Pitanje("Pitanje 45", "1. Izbaci uljeza:", listOf("NETAČNO", "NETAČNO", "NETAČNO", "TAČNO"), 4-1),
//            Pitanje("Pitanje 46", "2. Izbaci uljeza:", listOf("NETAČNO", "NETAČNO", "TAČNO", "NETAČNO"), 3-1),
//            Pitanje("Pitanje 47", "3. Izbaci uljeza:", listOf("NETAČNO", "TAČNO", "NETAČNO", "NETAČNO"), 2-1),
//            Pitanje("Pitanje 48", "4. Izbaci uljeza:", listOf("TAČNO", "NETAČNO", "NETAČNO", "NETAČNO"), 1-1),
//
//            Pitanje("Pitanje 49", "1. Izbaci uljeza:", listOf("NETAČNO", "NETAČNO", "NETAČNO", "TAČNO"), 4-1),
//            Pitanje("Pitanje 50", "2. Izbaci uljeza:", listOf("NETAČNO", "NETAČNO", "TAČNO", "NETAČNO"), 3-1),
//            Pitanje("Pitanje 51", "3. Izbaci uljeza:", listOf("NETAČNO", "TAČNO", "NETAČNO", "NETAČNO"), 2-1),
//            Pitanje("Pitanje 52", "4. Izbaci uljeza:", listOf("TAČNO", "NETAČNO", "NETAČNO", "NETAČNO"), 1-1),
//
//            Pitanje("Pitanje 53", "1. Izbaci uljeza:", listOf("NETAČNO", "NETAČNO", "NETAČNO", "TAČNO"), 4-1),
//            Pitanje("Pitanje 54", "2. Izbaci uljeza:", listOf("NETAČNO", "NETAČNO", "TAČNO", "NETAČNO"), 3-1),
//            Pitanje("Pitanje 55", "3. Izbaci uljeza:", listOf("NETAČNO", "TAČNO", "NETAČNO", "NETAČNO"), 2-1),
//            Pitanje("Pitanje 56", "4. Izbaci uljeza:", listOf("TAČNO", "NETAČNO", "NETAČNO", "NETAČNO"), 1-1),
//
//            Pitanje("Pitanje 57", "1. Izbaci uljeza:", listOf("NETAČNO", "NETAČNO", "NETAČNO", "TAČNO"), 4-1),
//            Pitanje("Pitanje 58", "2. Izbaci uljeza:", listOf("NETAČNO", "NETAČNO", "TAČNO", "NETAČNO"), 3-1),
//            Pitanje("Pitanje 59", "3. Izbaci uljeza:", listOf("NETAČNO", "TAČNO", "NETAČNO", "NETAČNO"), 2-1),
//            Pitanje("Pitanje 60", "4. Izbaci uljeza:", listOf("TAČNO", "NETAČNO", "NETAČNO", "NETAČNO"), 1-1)
//
//
//
//    )
//}