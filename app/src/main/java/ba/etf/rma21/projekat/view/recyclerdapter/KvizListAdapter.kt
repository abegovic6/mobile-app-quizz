package ba.etf.rma21.projekat.view.recyclerdapter

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Kviz
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*

class KvizListAdapter (
        private var kvizovi: List<Kviz>,
        private val onItemClicked :  (kviz : Kviz) -> Unit
)  : RecyclerView.Adapter<KvizListAdapter.KvizViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): KvizViewHolder {
        val view = LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_kviz, parent, false)
        return KvizViewHolder(view)
    }

    override fun getItemCount(): Int = kvizovi.size

    fun stringToDate(string: String?): Date?{
        if(string == null)
            return null
        val format = SimpleDateFormat("yyyy-MM-dd")
        return format.parse(string)
    }

    @SuppressLint("SetTextI18n")
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onBindViewHolder(holder: KvizViewHolder, position: Int) {
        holder.kvizPredmet.text = kvizovi[position].nazivPredmeta
        holder.kvizNaziv.text = kvizovi[position].naziv
        holder.kvizTrajanje.text = kvizovi[position].trajanje.toString() + " min"
        if(kvizovi[position].predan)
            holder.kvizBodovi.text = kvizovi[position].osvojeniBodovi.toString()
        else holder.kvizBodovi.text = ""

        var bojaMatch = "zelena"
        if(kvizovi[position].predan) {
            val d = Date()
            bojaMatch = if(stringToDate(kvizovi[position].datumPocetka)!!.before(d) && stringToDate(kvizovi[position].datumKraj)?.after(d) != false){
                "plava"
            } else if(stringToDate(kvizovi[position].datumPocetka)!!.after(d)){
                "zuta"
            } else {
                "crvena"
            }
        }

        val context: Context = holder.kvizStatus.context
        val id: Int = context.resources
                .getIdentifier(bojaMatch, "drawable", context.packageName)
        holder.kvizStatus.setImageResource(id)

        var d: Date? = Date()
        val formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy")
        when (bojaMatch) {
            "plava" -> d = stringToDate(kvizovi[position].datumRada)
            "zelena" -> d = stringToDate(kvizovi[position].datumKraj)
            "crvena" -> d = stringToDate(kvizovi[position].datumKraj)
            "zuta" -> d = stringToDate(kvizovi[position].datumPocetka)
        }

        if(d == null) {
            holder.kvizDatum.text = ""
        } else {
            val localDate : LocalDate = d.toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
            holder.kvizDatum.text = localDate.format(formatter)
        }

        holder.itemView.setOnClickListener{onItemClicked(kvizovi[position])}

    }

    fun updateKvizovi(kvizovi: List<Kviz>) {
        this.kvizovi = kvizovi
        notifyDataSetChanged()
    }



    inner class KvizViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val kvizPredmet : TextView = itemView.findViewById(R.id.kviz_predmet)
        val kvizStatus : ImageView = itemView.findViewById(R.id.kviz_status)
        val kvizNaziv : TextView = itemView.findViewById(R.id.kviz_naziv)
        val kvizDatum : TextView = itemView.findViewById(R.id.kviz_datum)
        val kvizTrajanje : TextView = itemView.findViewById(R.id.kviz_trajanje)
        val kvizBodovi : TextView = itemView.findViewById(R.id.kviz_bodovi)
    }

}