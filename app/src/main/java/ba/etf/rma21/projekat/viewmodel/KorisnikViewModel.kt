package ba.etf.rma21.projekat.viewmodel

import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.data.repositories.KorisnikRepository

class KorisnikViewModel {
    fun getMyKvizovi(): List<Kviz> {
        return KorisnikRepository.getMyKvizovi()
    }

    fun addPredmet (predmet: Predmet) {
        KorisnikRepository.addPredmet(predmet)
    }

    fun addGrupa (grupa: Grupa) {
        KorisnikRepository.addGrupa(grupa)
    }
}