package ba.etf.rma21.projekat.view.fragmenti

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.*
import android.widget.FrameLayout
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.text.getSpans
import androidx.core.text.toSpannable
import androidx.core.text.toSpanned
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import ba.etf.rma21.projekat.MainActivity
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.KvizTaken
import ba.etf.rma21.projekat.data.models.Odgovor
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.viewmodel.KvizTakenViewModel
import ba.etf.rma21.projekat.viewmodel.PitanjaKvizViewModel
import ba.etf.rma21.projekat.viewmodel.PitanjePokusajViewModel
import com.google.android.material.navigation.NavigationView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*


class FragmentPokusaj(private var pitanja : List<Pitanje>) : Fragment() {
    private lateinit var navigationView : NavigationView
    private lateinit var framePitanja : FrameLayout
    private lateinit var menuBottomNavigation: Menu
    private lateinit var kviz: Kviz
    private var menuItem: MenuItem? = null
    private var zavrsen : Boolean = false

    private lateinit var pitanjePokusajViewModel: PitanjePokusajViewModel
    private var pitanjaKvizViewModel = PitanjaKvizViewModel()

    private var kvizTakenViewModel = KvizTakenViewModel()

    private var  kvizTaken: KvizTaken = KvizTaken(0, "", 2F, null, 0)

    private var odgovori: List<Odgovor> = emptyList()
    private var ukupniBodovi : Float? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_pokusaj, container, false)

        navigationView = view.findViewById(R.id.navigacijaPitanja)
        framePitanja = view.findViewById(R.id.framePitanje)

        requireActivity().onBackPressedDispatcher.addCallback(this,
                object : OnBackPressedCallback(true) {
                    override fun handleOnBackPressed() {

                    }
                })

        val bundle = this.arguments
        if(bundle != null) {
            kviz = bundle.get("kviz") as Kviz
            zavrsen = kviz.predan
        }

        kvizTakenViewModel.zapocniKviz(idKviza = kviz.id!!, onSuccess = ::onSuccessKvizTaken, onError = ::onError)

        val activity = activity as MainActivity
        menuBottomNavigation = activity.getBottomNavigation().menu

        if (!zavrsen) {
            setUpMenu()
        }
        else {
            menuBottomNavigation.findItem(R.id.predajKviz).isChecked = true
        }


        pitanjePokusajViewModel = activity.run {
            ViewModelProviders.of(this)[PitanjePokusajViewModel::class.java]
        }

        pitanjaKvizViewModel.getOdgovori(id = kviz.id!!, onSuccess = ::onSuccessOdgovori, onError = ::onError, context = context!!)
        promijeniFramePitanje(0)


        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        pitanjePokusajViewModel.boja.observe(viewLifecycleOwner, Observer { boja ->
            if(menuItem != null) {
                val span = SpannableString(menuItem!!.title.toString())
                span.setSpan(
                        ForegroundColorSpan(Color.parseColor(boja)),
                        0,
                        span.length,
                        0
                )
                menuItem!!.title = span
            }
        })

    }

    private fun setUpMenu() {
        menuBottomNavigation.findItem(R.id.kvizovi).isVisible = false
        menuBottomNavigation.findItem(R.id.predmeti).isVisible = false
        menuBottomNavigation.findItem(R.id.predajKviz).isVisible = true
        menuBottomNavigation.findItem(R.id.zaustaviKviz).isVisible = true

        val zaustaviItemClickListener = MenuItem.OnMenuItemClickListener {
            zaustaviKviz()
            return@OnMenuItemClickListener true
        }
        menuBottomNavigation.findItem(R.id.zaustaviKviz).setOnMenuItemClickListener(zaustaviItemClickListener)

        val predajItemClickListener = MenuItem.OnMenuItemClickListener {
            predajKviz()
            return@OnMenuItemClickListener true
        }
        menuBottomNavigation.findItem(R.id.predajKviz).setOnMenuItemClickListener(predajItemClickListener)
    }


    fun onSuccessKvizTaken(kTaken: KvizTaken){
        GlobalScope.launch(Dispatchers.IO){
            withContext(Dispatchers.Main){
                kvizTaken = kTaken
                pitanjePokusajViewModel.id(kvizTaken.id!!)
            }
        }
    }

    fun onSuccessOdgovori(odg: List<Odgovor>){
        GlobalScope.launch(Dispatchers.IO){
            withContext(Dispatchers.Main){
                odgovori = odg
                setUpNavigationView()
            }
        }
    }

    fun onError() {
        val toast = Toast.makeText(context, "Search error", Toast.LENGTH_SHORT)
        toast.show()
    }


    @SuppressLint("ResourceType") // za group id kod menu items
    fun setUpNavigationView() {
        val menu = navigationView.menu
        var brojac = 0
        while (brojac < pitanja.size) {
            val item = menu.add(12345, brojac, brojac, (brojac + 1).toString())
            var odgovor: Odgovor? = null
                for(odg in odgovori) {
                    if(pitanja[brojac].id == odg.pitanjeId)
                        odgovor = odg
                }
            var zaPrvi = "#FFFFFF"
            if(odgovor != null) {
                val boja: String = if(odgovor.odgovoreno == pitanja[brojac].tacan) {
                    "#3DDC84"
                } else {
                    "#DB4F3D"
                }
                val span = SpannableString(item!!.title.toString())
                span.setSpan(ForegroundColorSpan(Color.parseColor(boja)),0, span.length,0)
                item.title = span
                if (brojac == 0) zaPrvi = boja
            }
            if(brojac == 0)
                pitanjePokusajViewModel.boja(zaPrvi)
            brojac++
        }

        if(zavrsen) menu.add(12345, brojac, brojac, "Rezultat")
        menuItem = menu[0]

        val mOnNavigationItemSelectedListener = NavigationView.OnNavigationItemSelectedListener { item ->
            if (item.itemId == brojac){
                otvoriRezultat()
            } else {
                promijeniFramePitanje(item.title.toString().toInt() - 1)
                menuItem = item
            }
            return@OnNavigationItemSelectedListener true
        }
        navigationView.setNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

    private fun promijeniFramePitanje(pozicija : Int){
        val fragment = FragmentPitanje.newInstance(pitanja[pozicija])
        val bundle = Bundle()
        bundle.apply {
            putBoolean("enabled", true) // PREPRAVITI
            putSerializable("kvizTaken", kvizTaken)
            putSerializable("kviz", kviz)
        }
        fragment.arguments = bundle
        openFragment(fragment)
    }

    private fun openFragment(fragment: Fragment) {
        val transaction = fragmentManager?.beginTransaction()
        if (transaction != null) {
            transaction.replace(R.id.framePitanje, fragment)
            transaction.addToBackStack(null)
            transaction.commit()
        }
    }

    private fun onSuccessUkupniBodovi(bodovi: Float?) {
        GlobalScope.launch(Dispatchers.IO){
            withContext(Dispatchers.Main){
                ukupniBodovi = bodovi
                otvoriRezultat1()
            }
        }
    }

    private fun otvoriRezultat() {
        pitanjaKvizViewModel.getUkupniBodovi(kviz = kviz, onSuccess = ::onSuccessUkupniBodovi, onError = ::onError, context = context!!)
    }

    private fun otvoriRezultat1() {
        val activity = activity as MainActivity
        menuBottomNavigation = activity.getBottomNavigation().menu
        menuBottomNavigation.findItem(R.id.kvizovi).isVisible = true
        menuBottomNavigation.findItem(R.id.predmeti).isVisible = true
        menuBottomNavigation.findItem(R.id.predajKviz).isVisible = false
        menuBottomNavigation.findItem(R.id.zaustaviKviz).isVisible = false
        menuBottomNavigation.findItem(R.id.predajKviz).isChecked = true

        menuItem = null

        val fragment = FragmentPoruka.newInstance()
        val bundle = Bundle()
        bundle.apply {
            if(ukupniBodovi != null)
                putString("tekst", "Završili ste kviz ${kviz.naziv} sa tačnosti ${ukupniBodovi}!")
            else putString("tekst", "Završili ste kviz ${kviz.naziv} sa tačnosti 0!")

        }
        fragment.arguments = bundle
        openFragment(fragment)
    }

    private fun predajKviz() {
        pitanjaKvizViewModel.odgovoriNaOstatak(kviz=kviz, onError = ::onError, onSuccess = ::onSuccessPredaj, context = context!!)
    }

    private fun onSuccessPredaj() {
        GlobalScope.launch(Dispatchers.IO){
            withContext(Dispatchers.Main){
                kviz.osvojeniBodovi = pitanjePokusajViewModel.bodovi.toFloat()

                navigationView.menu.add(12345, pitanja.size, pitanja.size, "Rezultat")
                zavrsen = true

                otvoriRezultat()
            }
        }
    }

    private fun zaustaviKviz() {
        pitanjePokusajViewModel.boja("#FFFFFF")
        val fragment = FragmentKvizovi.newInstance()
        val transaction = fragmentManager?.beginTransaction()
        if (transaction != null) {
            transaction.replace(R.id.container, fragment)
            transaction.addToBackStack(null)
            transaction.commit()
        }
    }

    companion object {
        fun newInstance(pitanja : List<Pitanje>) : FragmentPokusaj = FragmentPokusaj(pitanja)
    }
}