package ba.etf.rma21.projekat.data.repositories


import android.content.Context
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class DBRepository {

    companion object {

        private lateinit var context: Context

        fun setContext(_context: Context){
            context=_context
        }

        fun dajDate(date: LocalDateTime): String {
            var formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
            var formattedDate = date.format(formatter)
            formatter = DateTimeFormatter.ofPattern("HH:mm:ss")
            formattedDate += "T" + date.format(formatter)
            return formattedDate
        }

        suspend fun postaviKorisnika() {
            return withContext(Dispatchers.IO) {
                val db = AppDatabase.getInstance(context)
                izbrisiSve()
                for(odg in db.odgovorDAO().getSviOdgovori()) {
                    db.odgovorDAO().deleteOdgovori(odg)
                }
                val podaciKorisnik: List<Account>? = db.accountDAO().getAccount()
                if (podaciKorisnik != null && !podaciKorisnik.isEmpty())
                    db.accountDAO().deleteKorisnik(podaciKorisnik!!.first())
                var account = ApiAdapter.retrofit.getStudentAccount(AccountRepository.acHash)
                var formattedDate = dajDate(LocalDateTime.now())
                var formatted = "2021-05-26T15:36:55.90"
                if (account.message == null) {
                    db.accountDAO().insertAcc(Account(1, "", AccountRepository.acHash, formatted, ""))
                } else {
                    db.accountDAO().insertAcc(Account(1, "", AccountRepository.acHash, formatted, ""))
                }
            }
        }

        suspend fun izbrisiSve() {
            return withContext(Dispatchers.IO) {
                val db = AppDatabase.getInstance(context)
                for (kviz in db.kvizDAO().getKvizovi())
                    db.kvizDAO().deleteKvizovi(kviz)
                for(pitanje in db.pitanjeDAO().getSvaPitanja())
                    db.pitanjeDAO().deletePitanja(pitanje)
                /*for (odgovor in db.odgovorDAO().getSviOdgovori())
                    db.odgovorDAO().deleteOdgovori(odgovor)*/
                for (grupa in db.grupaDAO().getGrupe())
                    db.grupaDAO().deleteGrupe(grupa)
                for (predmet in db.predmetDAO().getPredmeti())
                    db.predmetDAO().deletePredmeti(predmet)
                for(kvizTaken in db.kvizTakenDAO().getKvizovTaken())
                    db.kvizTakenDAO().deleteKvizovTaken(kvizTaken)
            }
        }

        suspend fun ubaciSve() {
            return withContext(Dispatchers.IO) {
                val db = AppDatabase.getInstance(context)
                for (p in PredmetRepository.getUpisaniApi()) {
                    db.predmetDAO().insertPredmets(p)
                    for (g in GrupaRepository.getGroupsByPredmet(p.naziv)) {
                        g.nazivPredmeta = p.naziv
                        db.grupaDAO().insertGrupa(g)
                    }
                }
                var idPitanja = 0
                for (k in KvizRepository.getMyKvizes()) {
                    db.kvizDAO().insertKvizs(k)
                    val pitanja = PitanjeKvizRepository.getPitanjaApi(k.id)
                    if (pitanja != null)
                        for (p in pitanja) {
                            p.KvizId = k.id!!
                            p.idMoj = idPitanja
                            idPitanja++
                            db.pitanjeDAO().insertPitanja(p)
                        }
                }
                val kvizTaken = TakeKvizRepository.getPocetiKvizovi()
                if(kvizTaken != null)
                    for(k in kvizTaken)
                        db.kvizTakenDAO().insertKvizTaken(k)
            }
        }

        suspend fun updateNow() : Boolean {
            return withContext(Dispatchers.IO) {
                val db = AppDatabase.getInstance(context)

                var formattedDate = dajDate(LocalDateTime.now())

                val podaciKorisnik: List<Account>? = db.accountDAO().getAccount()
                if (podaciKorisnik == null) {
                    izbrisiSve()
                    ubaciSve()
                    db.accountDAO().insertAcc(Account(1, "", AccountRepository.acHash, formattedDate, ""))
                    return@withContext true
                }
                if (podaciKorisnik.isEmpty()) {
                    izbrisiSve()
                    ubaciSve()
                    db.accountDAO().insertAcc(Account(1, "", AccountRepository.acHash, formattedDate, ""))
                    return@withContext true
                }
                val changed = ApiAdapter.retrofit.changed(AccountRepository.acHash, podaciKorisnik.first().lastUpdate)

                if (!changed.changed!!)
                    return@withContext false

                AccountRepository.promijeni = false

                izbrisiSve()
                ubaciSve()
                db.accountDAO().deleteKorisnik(podaciKorisnik.first())
                db.accountDAO().insertAcc(Account(1, "", AccountRepository.acHash, formattedDate, ""))

                return@withContext true
            }
        }

        suspend fun getUpisaneKvizove() : List<Kviz> {
            return withContext(Dispatchers.IO) {
                val db = AppDatabase.getInstance(context)
                updateNow()
                return@withContext db.kvizDAO().getKvizovi()
            }
        }

        suspend fun getUpisanePredmete() : List<Predmet> {
            return withContext(Dispatchers.IO) {
                val db = AppDatabase.getInstance(context)
                updateNow()
                return@withContext db.predmetDAO().getPredmeti()
            }
        }

        suspend fun getPitanjaZaKviz(id: Int) : List<Pitanje> {
            return withContext(Dispatchers.IO) {
                val db = AppDatabase.getInstance(context)
                updateNow()
                return@withContext db.pitanjeDAO().getPitanja(id)
            }
        }

        suspend fun getPitanja() : List<Pitanje> {
            return withContext(Dispatchers.IO) {
                val db = AppDatabase.getInstance(context)
                updateNow()
                return@withContext db.pitanjeDAO().getSvaPitanja()
            }
        }

        suspend fun getKviz(id: Int) : Kviz? {
            return withContext(Dispatchers.IO) {
                val db = AppDatabase.getInstance(context)
                var kvizovi = db.kvizDAO().getKvizovi().firstOrNull{kviz -> kviz.id == id }
                //updateNow()
                return@withContext kvizovi
            }
        }

        suspend fun getOdgovori() : List<Odgovor> {
            return withContext(Dispatchers.IO) {
                val db = AppDatabase.getInstance(context)
                //updateNow()
                return@withContext db.odgovorDAO().getSviOdgovori()
            }
        }

        suspend fun dodajOdgovor(odgovor: Odgovor) {
            return withContext(Dispatchers.IO) {
                val db = AppDatabase.getInstance(context)
                //updateNow()
                return@withContext db.odgovorDAO().insertOdgovor(odgovor)
            }
        }

        suspend fun dodajKviz(kviz: Kviz) {
            return withContext(Dispatchers.IO) {
                val db = AppDatabase.getInstance(context)
                //updateNow()
                return@withContext db.kvizDAO().insertKvizs(kviz)
            }
        }

        suspend fun dodajKvizTaken(kviz: KvizTaken) {
            return withContext(Dispatchers.IO) {
                val db = AppDatabase.getInstance(context)
                //updateNow()
                return@withContext db.kvizTakenDAO().insertKvizTaken(kviz)
            }
        }

        suspend fun getKvizTaken(): List<KvizTaken> {
            return withContext(Dispatchers.IO) {
                val db = AppDatabase.getInstance(context)
                return@withContext db.kvizTakenDAO().getKvizovTaken()
            }
        }





    }


}