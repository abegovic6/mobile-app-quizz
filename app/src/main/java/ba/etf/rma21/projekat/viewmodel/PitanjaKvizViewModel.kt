package ba.etf.rma21.projekat.viewmodel

import android.content.Context
import ba.etf.rma21.projekat.data.models.*
import ba.etf.rma21.projekat.data.repositories.KvizRepository
import ba.etf.rma21.projekat.data.repositories.OdgovorRepository
import ba.etf.rma21.projekat.data.repositories.PitanjeKvizRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class PitanjaKvizViewModel {
    val scope = CoroutineScope(Job() + Dispatchers.IO)

    fun getPitanja(context: Context, id: Int, onSuccess: (pitanja: List<Pitanje>) -> Unit,
                   onError: () -> Unit) {
        scope.launch {
            PitanjeKvizRepository.setContext(context)
            val result = PitanjeKvizRepository.getPitanja(id)
            when (result) {
                is List<Pitanje> -> onSuccess.invoke(result)
                else -> onError.invoke()
            }
        }
    }

    fun getUkupniBodovi(context: Context, kviz: Kviz, onSuccess: (bodovi: Float?) -> Unit,
                   onError: () -> Unit) {
        scope.launch {
            KvizRepository.setContext(context)
            val result = KvizRepository.dajUkupneBodove(kviz)
            when (result) {
                is Float? -> onSuccess.invoke(result)
                else -> onError.invoke()
            }
        }
    }

    fun getOdgovori(context: Context, id: Int, onSuccess: (odgovori: List<Odgovor>) -> Unit,
                   onError: () -> Unit) {
        scope.launch {
            OdgovorRepository.setContext(context)
            val result = OdgovorRepository.getOdgovoriKviz(id)
            when (result) {
                is List<Odgovor> -> onSuccess.invoke(result)
                else -> onError.invoke()
            }
        }
    }

    fun getOdgovor(context: Context, idKviza: Int, idPitanja: Int, onSuccess: (odgovor: Odgovor?) -> Unit,
                   onError: () -> Unit) {
        scope.launch {
            var result: Odgovor? = null
            OdgovorRepository.setContext(context)
            val pomocna = OdgovorRepository.getOdgovoriKviz(idKviza)
            if (pomocna != null) {
                for (odgovor in pomocna) {
                    if(idPitanja == odgovor.pitanjeId)
                        result = odgovor
                }
            }
            when (result) {
                is Odgovor? -> onSuccess.invoke(result)
                else -> onError.invoke()
            }
        }
    }

    fun postaviOdgovorKviz(context: Context, idKviz: Int, idPitanje: Int, odgovor: Int, onSuccess: (bodovi: Int) -> Unit,
                   onError: () -> Unit) {
        scope.launch {
            OdgovorRepository.setContext(context)
            val result = OdgovorRepository.postaviOdgovorKviz(idKviz, idPitanje, odgovor)
            when (result) {
                is Int-> onSuccess.invoke(result)
                else -> onError.invoke()
            }
        }
    }


    fun odgovoriNaOstatak(context: Context, kviz: Kviz, onSuccess: () -> Unit,
                      onError: () -> Unit) {
        scope.launch {
            OdgovorRepository.setContext(context)
            val result = OdgovorRepository.predajOdgovore(kviz.id!!)
            when (result) {
                is Unit-> onSuccess.invoke()
                else -> onError.invoke()
            }
        }
    }

}