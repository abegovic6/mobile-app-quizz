package ba.etf.rma21.projekat.viewmodel

import android.content.ClipData
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class PitanjePokusajViewModel() : ViewModel() {
    private val mutableBoja = MutableLiveData<String>()
    private var mutableBodovi = -1
    private var mutableid = 0

    val boja: LiveData<String> get() = mutableBoja

    fun boja (item: String) {
        mutableBoja.value = item
    }

    val bodovi: Int get() = mutableBodovi

    fun bodovi (item: Int) {
        mutableBodovi = item
    }

    val id: Int get() = mutableid

    fun id (item: Int) {
        mutableid = item
    }



}