package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Predmet
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


class PredmetIGrupaRepository {
    companion object {
        fun setContext(_context: Context){
            DBRepository.setContext(_context)
        }

        suspend fun getPredmeti():List<Predmet> {
            return PredmetRepository.getAll()
        }

        suspend fun getGrupe():List<Grupa> {
            return GrupaRepository.grupe()
        }

        suspend fun getGrupeZaPredmet(idPredmeta:Int):List<Grupa> {
            return withContext(Dispatchers.IO) {
                val predmet = ApiAdapter.retrofit.getPredmetSaId(idPredmeta)
                return@withContext GrupaRepository.getGroupsByPredmet(predmet.naziv)
            }
        }

        suspend fun upisiUGrupu(idGrupa:Int):Boolean {
            val mess = GrupaRepository.upisiUGrupu(idGrupa)
            if(mess.message == "Grupa not found." || mess.message.contains("Ne postoji account gdje je hash=")) return false
            return true
        }

        suspend fun getUpisaneGrupe():List<Grupa> {
            return withContext(Dispatchers.IO) {
                return@withContext ApiAdapter.retrofit.getGrupeZaStudenta(AccountRepository.getHash())
            }
        }
    }
}