package ba.etf.rma21.projekat.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import ba.etf.rma21.projekat.data.dao.*
import ba.etf.rma21.projekat.data.models.Converters
import ba.etf.rma21.projekat.data.models.*

@Database(entities = arrayOf(Kviz::class, Predmet::class, Grupa::class, Pitanje::class, Account::class, Odgovor::class, KvizTaken::class), version = 1)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun kvizDAO() : KvizDAO
    abstract fun predmetDAO() : PredmetDAO
    abstract fun grupaDAO() : GrupaDAO
    abstract fun pitanjeDAO() : PitanjeDAO
    abstract fun odgovorDAO() : OdgovorDAO
    abstract fun accountDAO() : AccountDAO
    abstract fun kvizTakenDAO(): KvizTakenDAO
    companion object {
        private var INSTANCE: AppDatabase? = null
        fun getInstance(context: Context): AppDatabase {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = buildRoomDB(context)
                }
            }
            return INSTANCE!!
        }
        fun setInstance(appdb: AppDatabase):Unit{
            INSTANCE =appdb
        }
        private fun buildRoomDB(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                AppDatabase::class.java,
                "RMA21DB"
            ).build()
    }

}