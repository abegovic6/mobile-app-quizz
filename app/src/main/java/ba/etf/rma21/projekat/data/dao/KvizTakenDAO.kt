package ba.etf.rma21.projekat.data.dao

import androidx.room.*
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.KvizTaken

@Dao
interface KvizTakenDAO {
    @Query("SELECT * FROM KvizTaken")
    suspend fun getKvizovTaken(): List<KvizTaken>

    @Delete
    fun deleteKvizovTaken(k: KvizTaken)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertKvizTaken(vararg kvizovi: KvizTaken)

}