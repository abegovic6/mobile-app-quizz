package ba.etf.rma21.projekat.data.models


import retrofit2.http.*


interface Api {
    //za kvizove
    @GET("/kviz")
    suspend fun getAllKvizes(): List<Kviz>

    @GET("/kviz/{id}")
    suspend fun getKvizSaId(@Path("id") groupId:Int): Kviz

    @GET("/grupa/{id}/kvizovi")
    suspend fun getKvizoveZaIdGrupe(@Path("id") groupId:Int): List<Kviz>

    //za predmete
    @GET("/predmet/{id}")
    suspend fun getPredmetSaId(@Path("id") predmetId: Int): Predmet

    @GET("/predmet")
    suspend fun getAllPredmeti(): List<Predmet>

    // za kvizTaken
    @GET("/student/{id}/kviztaken")
    suspend fun getKvizTakenZaStudenta(@Path("id") hashStudent:String): List<KvizTaken>

    @POST("/student/{id}/kviz/{kid}")
    suspend fun zapocniKviz(@Path("id") hash: String, @Path("kid")idKviz: Int) : KvizTaken


    //za odgovor
    @GET("/student/{id}/kviztaken/{ktid}/odgovori")
    suspend fun getOdgovorZaKviz(@Path("id") studentId: String, @Path("ktid") kvizId:Int): List<Odgovor>

    @POST("/student/{id}/kviztaken/{kid}/odgovor")
    suspend fun postaviOdgovor(@Path("id") hash: String, @Path("kid")idKvizTaken: Int, @Body odgovor: OdgovorenoPitanje)

    //za studenta
    @GET("/student/{id}")
    suspend fun getStudentAccount(@Path("id") hashId:String): Account

    //za pitanje
    @GET("/kviz/{id}/pitanja")
    suspend fun getPitanjaForQuiz(@Path("id") kvizId:Int): List<Pitanje>

    //za grupe
    @GET("/grupa")
    suspend fun getAllGroups(): List<Grupa>

    @GET("/grupa/{id}")
    suspend fun getGrupaSaId(@Path("id") groupId:Int): Grupa

    @GET("/predmet/{id}/grupa")
    suspend fun getGrupaZaPredmet(@Path("id") groupId:Int): List<Grupa>

    @GET("/kviz/{id}/grupa")
    suspend fun getGrupeZaKviz(@Path("id") groupId:Int): List<Grupa>

    // student
    @GET("/student/{id}/grupa")
    suspend fun getGrupeZaStudenta(@Path("id") hashStudent:String): List<Grupa>

    @POST("/grupa/{gid}/student/{id}")
    suspend fun upisiUGrupu(@Path("gid") idGrupe: Int, @Path("id") hashStudent: String) : Message

    @GET("/account/{id}/lastUpdate?=date")
    suspend fun changed(@Path("id") id:String, @Query("date") date:String): Changed

}