package ba.etf.rma21.projekat.view.arrayadapter

import android.graphics.Color
import android.view.View
import android.widget.AdapterView
import android.widget.TextView
import androidx.core.view.children
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.viewmodel.PitanjePokusajViewModel

class OdgovoriListAdapter(
    val pitanje: Pitanje,
    private val viewModel : PitanjePokusajViewModel,
    private val funckijaOnClick: (pozicija : Int) -> Unit
) : AdapterView.OnItemClickListener {


    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        if(position == pitanje.tacan) {
            println("udje position == tacan")
            val textView = parent?.getChildAt(position) as TextView
            textView.setBackgroundColor(Color.parseColor(("#3DDC84")))
            viewModel.boja("#3DDC84")
        } else if(position != pitanje.opcije.size) {
            println("udje position != tacan")
            var textView = parent?.getChildAt(position) as TextView
            textView.setBackgroundColor(Color.parseColor(("#DB4F3D")))
            textView = parent.getChildAt(pitanje.tacan) as TextView
            textView.setBackgroundColor(Color.parseColor(("#3DDC84")))
            viewModel.boja("#DB4F3D")
        }

        for (child in parent!!.children){
            child.isEnabled = false
            child.setOnClickListener(null)
        }

        funckijaOnClick(position)

    }


}