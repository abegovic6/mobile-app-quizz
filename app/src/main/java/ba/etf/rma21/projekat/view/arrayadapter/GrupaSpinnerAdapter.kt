package ba.etf.rma21.projekat.view.arrayadapter

import android.view.View
import android.widget.AdapterView

class GrupaSpinnerAdapter (
    private val funckijaOnClick: () -> Unit) : AdapterView.OnItemSelectedListener {

    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        funckijaOnClick()

    }

    override fun onNothingSelected(parent: AdapterView<*>) {
        // write code to perform some action
    }


}