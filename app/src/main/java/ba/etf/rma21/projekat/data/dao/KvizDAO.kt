package ba.etf.rma21.projekat.data.dao

import androidx.room.*
import ba.etf.rma21.projekat.data.models.Kviz

@Dao
interface KvizDAO {
    @Query("SELECT * FROM kviz")
    suspend fun getKvizovi(): List<Kviz>

    @Delete
    fun deleteKvizovi(k: Kviz)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertKvizs(vararg kvizovi: Kviz)

}