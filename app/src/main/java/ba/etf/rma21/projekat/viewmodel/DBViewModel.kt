package ba.etf.rma21.projekat.viewmodel

import android.content.Context
import ba.etf.rma21.projekat.data.repositories.AccountRepository
import ba.etf.rma21.projekat.data.repositories.DBRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class DBViewModel {
    val scope = CoroutineScope(Job() + Dispatchers.IO)

    fun uradiUpdate(context: Context, onSuccess: (postavljeno: Boolean) -> Unit,
                    onError: () -> Unit) {
        scope.launch {
            DBRepository.setContext(context)
            val result = DBRepository.updateNow()
            when (result) {
                is Boolean -> onSuccess.invoke(result)
                else -> onError.invoke()
            }
        }
    }

}