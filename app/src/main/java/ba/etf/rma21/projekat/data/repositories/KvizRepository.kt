package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.models.Kviz
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.util.*



class KvizRepository {

    companion object {

        fun setContext(_context: Context){
            DBRepository.setContext(_context)
        }

        suspend fun dajUkupneBodove(kviz: Kviz) : Float? {
            return withContext(Dispatchers.IO) {
                var vrati = DBRepository.getKviz(kviz.id!!)
                if(vrati == null || vrati.osvojeniBodovi == null) return@withContext 0F
                return@withContext vrati.osvojeniBodovi
            }
        }

        fun stringToDate(string: String?): Date?{
            if(string == null)
                return null
            val format = SimpleDateFormat("yyyy-MM-dd")
            return format.parse(string)
        }



        suspend fun getMyKvizes(
        ) : List<Kviz>{
            return withContext(Dispatchers.IO) {
                var hash = AccountRepository.getHash()
                var response = ApiAdapter.retrofit.getGrupeZaStudenta(hash)
                var povrat = mutableListOf<Kviz>()
                for(grupa in response) {
                    var responseKviz = ApiAdapter.retrofit.getKvizoveZaIdGrupe(grupa.id)
                    var imenaPredmeta = mutableListOf<String>()
                    for(kviz in responseKviz){
                        var predmet = ApiAdapter.retrofit.getPredmetSaId(grupa.predmetId).naziv
                        if (kviz.nazivPredmeta == null)
                            kviz.nazivPredmeta = predmet
                        else
                            kviz.nazivPredmeta += ", " + predmet
                        kviz.predan = OdgovorRepository.daLiJeZavrsen(kviz)
                        povrat.add(kviz)
                    }
                }
                val sviKvizTaken = ApiAdapter.retrofit.getKvizTakenZaStudenta(AccountRepository.getHash())
                for(kvizTaken in sviKvizTaken) {
                    for(kviz in povrat) {
                        if(kvizTaken.KvizId == kviz.id) {
                            kviz.datumRada = kvizTaken.datumRada
                            kviz.osvojeniBodovi = kvizTaken.osvojeniBodovi.toFloat() //mozda treba int ko zna
                        }
                    }
                }
                return@withContext povrat
            }
        }


        suspend fun getDone(): List<Kviz> {
            return getUpisani().filter { kviz ->  kviz.datumRada != null}
        }

        suspend fun getFuture(): List<Kviz> {
            val d = Date()
            return getUpisani().filter { kviz ->  stringToDate(kviz.datumPocetka)!!.after(d)}
        }

        suspend fun getNotTaken(): List<Kviz> {
            val d = Date()
            return getUpisani().filter { kviz ->  stringToDate(kviz.datumKraj)?.before(d) ?: false && kviz.datumRada == null}
        }

        fun getStatus(kviz: Kviz) : String {
            val d = Date()
            if(stringToDate(kviz.datumKraj)?.before(d) == true && kviz.datumRada == null) return "crvena"
            if(stringToDate(kviz.datumPocetka)!!.after(d)) return "zuta"
            if(kviz.datumRada != null) return "plava"
            return "zelena"
        }

        suspend fun getAll() : List<Kviz>?
        {
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getAllKvizes()
                var povrat = mutableListOf<Kviz>()
                for(kviz in response) {
                    var responseGrupe = ApiAdapter.retrofit.getGrupeZaKviz(kviz.id!!)
                    var imenaPredmeta = mutableListOf<String>()
                    for(grupa in responseGrupe){
                        var predmet = ApiAdapter.retrofit.getPredmetSaId(grupa.predmetId).naziv
                        if(!imenaPredmeta.contains(predmet)) {
                            if (kviz.nazivPredmeta == "" || kviz.nazivPredmeta == null)
                                kviz.nazivPredmeta = predmet
                            else
                                kviz.nazivPredmeta += ", " + predmet
                            imenaPredmeta.add(predmet)
                        }
                    }
                    kviz.predan = OdgovorRepository.daLiJeZavrsen(kviz)
                    povrat.add(kviz)
                }
                val sviKvizTaken = ApiAdapter.retrofit.getKvizTakenZaStudenta(AccountRepository.getHash())
                for(kvizTaken in sviKvizTaken) {
                    for(kviz in povrat) {
                        if(kvizTaken.KvizId == kviz.id) {
                            kviz.datumRada = kvizTaken.datumRada
                            kviz.osvojeniBodovi = kvizTaken.osvojeniBodovi.toFloat() //mozda treba int ko zna
                        }
                    }
                }
                if(povrat.isEmpty())
                    return@withContext emptyList<Kviz>()
                return@withContext povrat
            }
        }


        suspend fun getById (id: Int): Kviz? {
            return withContext(Dispatchers.IO) {
                return@withContext ApiAdapter.retrofit.getKvizSaId(id)

            }
        }

        suspend fun getUpisani(): List<Kviz> {
            return withContext(Dispatchers.IO) {
                return@withContext DBRepository.getUpisaneKvizove()
            }
        }

    }
}