package ba.etf.rma21.projekat.data.dao

import androidx.room.*
import ba.etf.rma21.projekat.data.models.Predmet

@Dao
interface PredmetDAO {
    @Query("SELECT * FROM predmet")
    suspend fun getPredmeti(): List<Predmet>

    @Delete
    fun deletePredmeti(predmet: Predmet)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPredmets(vararg kvizovi: Predmet)
}