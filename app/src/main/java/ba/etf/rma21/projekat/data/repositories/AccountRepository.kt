package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class AccountRepository {
    companion object {
        var acHash : String = "15c1b448-c803-47e3-866b-1fcff0e8e651"
        var promijeni: Boolean = false

        fun setContext(_context: Context){
            DBRepository.setContext(_context)
        }

        suspend fun postaviHash(accountHash:String):Boolean {
            return withContext(Dispatchers.IO) {
                if(acHash != accountHash) promijeni = true
                acHash = accountHash
                //DBRepository.updateNow()
                DBRepository.postaviKorisnika()
                return@withContext true
            }
        }

        fun getHash():String {
            return acHash
        }
    }

}