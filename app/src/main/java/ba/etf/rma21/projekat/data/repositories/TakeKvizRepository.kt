package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.models.KvizTaken
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class TakeKvizRepository{
    companion object {

        fun setContext(_context: Context){
            DBRepository.setContext(_context)
        }

        suspend fun zapocniKviz(idKviza: Int?): KvizTaken? {
            return withContext(Dispatchers.IO) {
                var vrati: KvizTaken? = null
                var lista = DBRepository.getKvizTaken()
                if (!lista.isEmpty()) {
                    for (kviztaken in lista) {
                        if (kviztaken.KvizId == idKviza) {
                            vrati = kviztaken
                            break
                        }
                    }
                }
                if (vrati == null) {
                    vrati = ApiAdapter.retrofit.zapocniKviz(AccountRepository.getHash(), idKviza!!)
                    vrati.KvizId = idKviza
                    DBRepository.dodajKvizTaken(vrati)
                }
                return@withContext vrati
            }
        }

        suspend fun getPocetiKvizovi():List<KvizTaken>? {
            return withContext(Dispatchers.IO) {
                val vrati = ApiAdapter.retrofit.getKvizTakenZaStudenta(AccountRepository.getHash())
                if (vrati.isEmpty())
                    return@withContext null
                else return@withContext vrati
            }
        }

    }


}